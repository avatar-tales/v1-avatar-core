package fr.avatarreturns.avatarcore.integrations;

import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.flags.Flag;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.flags.registry.FlagConflictException;
import com.sk89q.worldguard.protection.flags.registry.FlagRegistry;
import fr.avatarreturns.api.AvatarReturnsAPI;
import fr.avatarreturns.avatarcore.AvatarCore;
import fr.avatarreturns.avatarcore.listeners.factions.ChangePower;

import java.util.HashMap;
import java.util.Map;

public class WorldGuardIntegration {

    private static WorldGuardIntegration instance;

    public static WorldGuardIntegration getInstance() {
        if (instance == null)
            instance = new WorldGuardIntegration();
        return instance;
    }

    public final Map<String, StateFlag> flags;

    private WorldGuardIntegration() {
        this.flags = new HashMap<>();
        if (AvatarReturnsAPI.get().isIntegrate("Factions"))
            AvatarCore.get().getPlugin().getServer().getPluginManager().registerEvents(new ChangePower(), AvatarCore.get().getPlugin());

    }

    public void createFlag(final String name) {
        FlagRegistry registry = WorldGuard.getInstance().getFlagRegistry();
        try {
            StateFlag flag = new StateFlag(name, true);
            registry.register(flag);
            flags.put(name, flag);
        } catch (FlagConflictException e) {
            Flag<?> existing = registry.get(name);
            if (existing instanceof StateFlag) {
                flags.put(name, (StateFlag) existing);
            }
        }
    }

    public StateFlag getFlag(final String name) {
        return this.flags.get(name);
    }
}
