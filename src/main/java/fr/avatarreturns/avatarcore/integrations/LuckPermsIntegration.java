package fr.avatarreturns.avatarcore.integrations;

import net.luckperms.api.LuckPermsProvider;
import net.luckperms.api.model.user.User;
import net.luckperms.api.node.Node;

import java.util.Optional;
import java.util.UUID;

public class LuckPermsIntegration {

    private static LuckPermsIntegration instance;

    public static LuckPermsIntegration getInstance() {
        if (instance == null)
            instance = new LuckPermsIntegration();
        return instance;
    }

    private LuckPermsIntegration(){
    }

    public String  getGroupName(UUID playerUuid) {
        return LuckPermsProvider.get().getUserManager().getUser(playerUuid).getPrimaryGroup();
    }

    public void addPermissions(final UUID uuid, final String... permissions) {
        final Optional<User> user = Optional.ofNullable(LuckPermsProvider.get().getUserManager().getUser(uuid));
        if (user.isPresent()) {
            for (final String permission : permissions) {
                user.get().data().add(Node.builder(permission).build());
            }
        }
    }

    public void removePermissions(final UUID uuid, final String... permissions) {
        final Optional<User> user = Optional.ofNullable(LuckPermsProvider.get().getUserManager().getUser(uuid));
        if (user.isPresent()) {
            for (final String permission : permissions) {
                user.get().data().remove(Node.builder(permission).build());
            }
        }
    }
}
