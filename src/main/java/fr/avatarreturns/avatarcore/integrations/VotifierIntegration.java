package fr.avatarreturns.avatarcore.integrations;

import fr.avatarreturns.api.AvatarReturnsAPI;
import fr.avatarreturns.avatarcore.commands.votifier.Vote;

public class VotifierIntegration {

    private static VotifierIntegration instance;

    public static VotifierIntegration getInstance() {
        if (instance == null)
            instance = new VotifierIntegration();
        return instance;
    }

    private VotifierIntegration() {
        AvatarReturnsAPI.get().registerCommandClass(new Vote());
        AvatarReturnsAPI.get().getPlugin().getServer().getPluginManager().registerEvents(new fr.avatarreturns.avatarcore.listeners.votifier.Vote(), AvatarReturnsAPI.get().getPlugin());
    }

}
