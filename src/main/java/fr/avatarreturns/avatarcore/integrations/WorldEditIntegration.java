package fr.avatarreturns.avatarcore.integrations;

public class WorldEditIntegration {

    private static WorldEditIntegration instance;

    public static WorldEditIntegration getInstance() {
        if (instance == null)
            instance = new WorldEditIntegration();
        return instance;
    }

    private WorldEditIntegration(){
    }
}
