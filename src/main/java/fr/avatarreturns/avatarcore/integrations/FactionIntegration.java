package fr.avatarreturns.avatarcore.integrations;

import com.massivecraft.factions.Rel;
import com.massivecraft.factions.entity.Faction;
import com.massivecraft.factions.entity.FactionColl;
import com.massivecraft.factions.entity.MPlayer;
import fr.avatarreturns.api.AvatarReturnsAPI;
import fr.avatarreturns.avatarcore.commands.factions.FactionHome;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class FactionIntegration {

    private static FactionIntegration instance;

    public static FactionIntegration getInstance() {
        if (instance == null)
            instance = new FactionIntegration();
        return instance;
    }

    private FactionIntegration(){
        AvatarReturnsAPI.get().registerCommandClass(new FactionHome());
    }

    @Nullable
    public String getFactionName(final UUID uuid) {
        final MPlayer mplayer = MPlayer.get(uuid);
        if (mplayer == null)
            return "Wild";
        if (mplayer.getFactionName() == null)
            return "Wild";
        if (mplayer.getFactionName().equalsIgnoreCase(""))
            return "Wild";
        return mplayer.getFactionName();
    }

    public List<Player> getFactionOnlinePlayers(final String name) {
        final Faction faction = FactionColl.get().getByName(name);
        if (faction == null)
            return new ArrayList<>();
        return AvatarReturnsAPI.get().getUtils().copy((faction.getOnlinePlayers() == null ? new ArrayList<>() : faction.getOnlinePlayers()));
    }

    public List<Player> getFactionAlliesOnlinePlayers(final String name) {
        final Faction faction = FactionColl.get().getByName(name);
        if (faction == null)
            return new ArrayList<>();
        final List<Player> players = getFactionOnlinePlayers(name);
        for (final Faction factions : FactionColl.get().getAll()) {
            if (faction.getRelationTo(factions).equals(Rel.ALLY))
                players.addAll(factions.getOnlinePlayers());
        }
        return players;
    }

    public List<Player> getFactionTruceOnlinePlayers(final String name) {
        final Faction faction = FactionColl.get().getByName(name);
        if (faction == null)
            return new ArrayList<>();
        final List<Player> players = getFactionOnlinePlayers(name);
        for (final Faction factions : FactionColl.get().getAll()) {
            if (faction.getRelationTo(factions).equals(Rel.TRUCE))
                players.addAll(factions.getOnlinePlayers());
        }
        return players;
    }

    public String getPlayerTitle(final UUID uuid) {
        final MPlayer mPlayer = MPlayer.get(uuid);
        return (mPlayer.getTitle().contains("no title set") ? "" : mPlayer.getTitle());
    }

    public String getPlayerRank(final UUID uuid) {
        final MPlayer mPlayer = MPlayer.get(uuid);
        return mPlayer.getRank().getName();
    }
}
