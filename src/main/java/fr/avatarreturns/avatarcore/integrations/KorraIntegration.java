package fr.avatarreturns.avatarcore.integrations;

import com.projectkorra.projectkorra.BendingPlayer;
import com.projectkorra.projectkorra.Element;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class KorraIntegration {

    private static KorraIntegration instance;

    public static KorraIntegration getInstance() {
        if (instance == null)
            instance = new KorraIntegration();
        return instance;
    }

    private KorraIntegration(){
    }

    public List<String> getPlayerBend(final UUID uuid) {
        BendingPlayer bendingPlayer = BendingPlayer.getBendingPlayer(Bukkit.getOfflinePlayer(uuid));
        return bendingPlayer.getElements().stream().map(Element::getName).collect(Collectors.toList());
    }

}
