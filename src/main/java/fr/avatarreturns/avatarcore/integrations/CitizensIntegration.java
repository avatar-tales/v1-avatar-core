package fr.avatarreturns.avatarcore.integrations;

public class CitizensIntegration {

    private static CitizensIntegration instance;

    public static CitizensIntegration getInstance() {
        if (instance == null)
            instance = new CitizensIntegration();
        return instance;
    }

    private CitizensIntegration() {
    }
}
