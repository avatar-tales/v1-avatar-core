package fr.avatarreturns.avatarcore.integrations.vault;

import fr.avatarreturns.api.AvatarReturnsAPI;
import fr.avatarreturns.avatarcore.AvatarCorePlugin;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.plugin.RegisteredServiceProvider;

import java.util.UUID;

public class VaultIntegration {

    private static VaultIntegration instance;

    public static VaultIntegration getInstance() {
        if (instance == null)
            instance = new VaultIntegration();
        return instance;
    }

    private Economy economy;

    private VaultIntegration(){
    }

    public boolean searchEconomy() {
        RegisteredServiceProvider<Economy> rsp = AvatarCorePlugin.get().getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            rsp = VaultHook.hook();
            if (rsp == null)
                return false;
        }
        economy = rsp.getProvider();
        return economy != null;
    }

    public void addMoney(final UUID uuid, final double money) {
        final OfflinePlayer player = Bukkit.getOfflinePlayer(uuid);
        if (player == null)
            return;
        AvatarReturnsAPI.get().debug("Add money $" + money + " to " + player.getName());
        economy.depositPlayer(player, money);
    }

    public void removeMoney(final UUID uuid, final double money) {
        final OfflinePlayer player = Bukkit.getOfflinePlayer(uuid);
        if (player == null)
            return;
        AvatarReturnsAPI.get().debug("Remove money $" + money + " to " + player.getName());
        if (economy.getBalance(player) <= money)
            economy.withdrawPlayer(player, economy.getBalance(player));
        else
            economy.withdrawPlayer(player, money);
    }

    public double getMoney(final UUID uuid) {
        final OfflinePlayer player = Bukkit.getOfflinePlayer(uuid);
        if (player == null)
            return 0;
        AvatarReturnsAPI.get().debug("Get money of " + player.getName());
        return economy.getBalance(player);
    }
}
