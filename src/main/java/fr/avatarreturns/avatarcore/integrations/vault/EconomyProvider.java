package fr.avatarreturns.avatarcore.integrations.vault;

import fr.avatarreturns.api.AvatarReturnsAPI;
import fr.avatarreturns.api.storage.databases.IDatabase;
import fr.avatarreturns.avatarcore.AvatarCorePlugin;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.FileFilter;
import java.sql.ResultSet;
import java.util.*;
import java.util.stream.Collectors;

public class EconomyProvider implements Economy {

    private static EconomyProvider instance;

    public static EconomyProvider get() {
        if (instance == null)
            instance = new EconomyProvider();
        return instance;
    }

    private final Map<String, Double> bank;
    private final JavaPlugin plugin;
    private final IDatabase database;

    private EconomyProvider() {
        this.bank = new HashMap<>();
        this.plugin = AvatarCorePlugin.get();
        this.database = AvatarReturnsAPI.get().getDatabaseCreator().createSQLiteDatabase(this.plugin, this.plugin.getDataFolder().getAbsolutePath(), "Economy.sql");
        this.database.modifyQuery( "CREATE TABLE IF NOT EXISTS `economy` (" +
                "`uuid` VARCHAR(36) PRIMARY KEY NOT NULL," +
                "`money` DOUBLE" +
                ");"
        );
        final File essentialsFolder = new File(this.plugin.getDataFolder().getAbsolutePath() + "/../Essentials/", "userdata");
        AvatarReturnsAPI.get().debug(essentialsFolder.getAbsolutePath());
        if (essentialsFolder.exists()) {
            try {
                final File isConverted = new File(essentialsFolder.getAbsolutePath(), ".converted");
                if (isConverted.exists())
                    return;
                final FileFilter filter = pathname -> pathname.getName().endsWith(".yml");
                UUID uuid;
                YamlConfiguration config;
                for (final File file : essentialsFolder.listFiles(filter)) {
                    try {
                        uuid = UUID.fromString(file.getName().replace(".yml", ""));
                        AvatarReturnsAPI.get().debug("Converting money from " + uuid.toString());
                        config = YamlConfiguration.loadConfiguration(file);
                        database.modifyQuery("INSERT INTO `economy` (`uuid`, `money`) VALUES ('" + uuid.toString() + "', '" + config.getDouble("money") +"');");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                isConverted.createNewFile();
            } catch (Exception ignored) {
            }
        }
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public String getName() {
        return "AvatarEconomy";
    }

    @Override
    public boolean hasBankSupport() {
        return false;
    }

    @Override
    public int fractionalDigits() {
        return 2;
    }

    @Override
    public String format(double v) {
        return v + "Y";
    }

    @Override
    public String currencyNamePlural() {
        return "Y";
    }

    @Override
    public String currencyNameSingular() {
        return "Y";
    }

    @Override
    public boolean hasAccount(String s) {
        return false;
    }

    @Override
    public boolean hasAccount(OfflinePlayer offlinePlayer) {
        return false;
    }

    @Override
    public boolean hasAccount(String s, String s1) {
        return false;
    }

    @Override
    public boolean hasAccount(OfflinePlayer offlinePlayer, String s) {
        return false;
    }

    @Override
    public double getBalance(final String name) {
        final Optional<UUID> uuid = AvatarReturnsAPI.get().getUtils().getRealUUID(name);
        if (!uuid.isPresent()) {
            try (final ResultSet resultSet = database.readQuery("SELECT * FROM `economy` WHERE `uuid` = '" + name + "';").get()) {
                double result = 0;
                if (resultSet.next())
                    result = resultSet.getDouble("money");
                return result;
            } catch (Exception ignored) {
            }
            return 0;
        }
        if (this.bank.containsKey(uuid.get().toString())) {
            return this.bank.get(uuid.get().toString());
        } else {
            try (final ResultSet resultSet = database.readQuery("SELECT * FROM `economy` WHERE `uuid` = '" + uuid.get().toString() + "';").get()) {
                double result = 0;
                if (resultSet.next())
                    result = resultSet.getDouble("money");
                if (Bukkit.getOfflinePlayer(uuid.get()).isOnline())
                    this.bank.put(uuid.get().toString(), result);
                return result;
            } catch (Exception ignored) {
            }
        }
        return 0;
    }

    @Override
    public double getBalance(final OfflinePlayer offlinePlayer) {
        if (this.bank.containsKey(offlinePlayer.getUniqueId().toString())) {
            return this.bank.get(offlinePlayer.getUniqueId().toString());
        } else {
            try (final ResultSet resultSet = database.readQuery("SELECT * FROM `economy` WHERE `uuid` = '" + offlinePlayer.getUniqueId().toString() + "';").get()) {
                double result = 0;
                if (resultSet.next())
                    result = resultSet.getDouble("money");
                if (offlinePlayer.isOnline())
                    this.bank.put(offlinePlayer.getUniqueId().toString(), result);
                return result;
            } catch (Exception ignored) {
            }
        }
        return 0;
    }

    @Override
    public double getBalance(final String name, final String s1) {
        return this.getBalance(name);
    }

    @Override
    public double getBalance(final OfflinePlayer offlinePlayer, final String s) {
        return this.getBalance(offlinePlayer);
    }

    @Override
    public boolean has(String s, double v) {
        return false;
    }

    @Override
    public boolean has(OfflinePlayer offlinePlayer, double v) {
        return false;
    }

    @Override
    public boolean has(String s, String s1, double v) {
        return false;
    }

    @Override
    public boolean has(OfflinePlayer offlinePlayer, String s, double v) {
        return false;
    }

    @Override
    public EconomyResponse withdrawPlayer(final String name, final double v) {
        final Optional<UUID> uuid = AvatarReturnsAPI.get().getUtils().getRealUUID(name);
        if (!uuid.isPresent()) {
            try (final ResultSet resultSet = database.readQuery("SELECT * FROM `economy` WHERE `uuid` = '" + name + "';").get()) {
                double result = 0;
                if (resultSet.next()) {
                    result = resultSet.getDouble("money");
                    database.modifyQuery("UPDATE `economy` SET `money` = '" + (result - v) + "' WHERE `uuid` = '" + name + "'");
                } else {
                    database.modifyQuery("INSERT INTO `economy` (`uuid`, `money`) VALUES ('" + name + "', '" + (0 - v) + "');");
                }
                return null;
            } catch (Exception ignored) {
            }
        }
        if (this.bank.containsKey(uuid.get().toString())) {
            final double oldBalance = this.bank.get(uuid.get().toString());
            this.bank.replace(uuid.get().toString(), oldBalance - v);
        } else {
            try (final ResultSet resultSet = database.readQuery("SELECT * FROM `economy` WHERE `uuid` = '" + uuid.get().toString() + "';").get()) {
                double result = 0;
                final boolean isSet = resultSet.next();
                if (isSet)
                    result = resultSet.getDouble("money");
                if (Bukkit.getOfflinePlayer(uuid.get()).isOnline())
                    this.bank.put(uuid.get().toString(), result - v);
                else if (isSet)
                    database.modifyQuery("UPDATE `economy` SET `money` = '" + (result - v) + "' WHERE `uuid` = '" + uuid.get().toString() + "'");
                else {
                    database.modifyQuery("INSERT INTO `economy` (`uuid`, `money`) VALUES ('" + name + "', '" + (result - v) + "');");
                }
                return null;
            } catch (Exception ignored) {
            }
        }
        return null;
    }

    @Override
    public EconomyResponse withdrawPlayer(final OfflinePlayer offlinePlayer, final double v) {
        if (this.bank.containsKey(offlinePlayer.getUniqueId().toString())) {
            final double oldBalance = this.bank.get(offlinePlayer.getUniqueId().toString());
            this.bank.replace(offlinePlayer.getUniqueId().toString(), oldBalance - v);
        } else {
            try (final ResultSet resultSet = database.readQuery("SELECT * FROM `economy` WHERE `uuid` = '" + offlinePlayer.getUniqueId().toString() + "';").get()) {
                double result = 0;
                final boolean isSet = resultSet.next();
                if (isSet)
                    result = resultSet.getDouble("money");
                if (offlinePlayer.isOnline())
                    this.bank.put(offlinePlayer.getUniqueId().toString(), result - v);
                else if (isSet)
                    database.modifyQuery("UPDATE `economy` SET `money` = '" + (result - v) + "' WHERE `uuid` = '" + offlinePlayer.getUniqueId().toString() + "'");
                else {
                    database.modifyQuery("INSERT INTO `economy` (`uuid`, `money`) VALUES ('" + offlinePlayer.getUniqueId().toString() + "', '" + (result - v) + "');");
                }
                return null;
            } catch (Exception ignored) {
            }
        }
        return null;
    }

    @Override
    public EconomyResponse withdrawPlayer(final String name, String s1, final double v) {
        return this.withdrawPlayer(name, v);
    }

    @Override
    public EconomyResponse withdrawPlayer(final OfflinePlayer offlinePlayer, final String s, final double v) {
        return this.withdrawPlayer(offlinePlayer, v);
    }

    @Override
    public EconomyResponse depositPlayer(final String name, final double v) {
        final Optional<UUID> uuid = AvatarReturnsAPI.get().getUtils().getRealUUID(name);
        if (!uuid.isPresent()) {
            try (final ResultSet resultSet = database.readQuery("SELECT * FROM `economy` WHERE `uuid` = '" + name + "';").get()) {
                double result = 0;
                if (resultSet.next()) {
                    result = resultSet.getDouble("money");
                    database.modifyQuery("UPDATE `economy` SET `money` = '" + (result + v) + "' WHERE `uuid` = '" + name + "'");
                } else {
                    database.modifyQuery("INSERT INTO `economy` (`uuid`, `money`) VALUES ('" + name + "', '" + (result - v) + "');");
                }
                return null;
            } catch (Exception ignored) {
            }
        }
        if (this.bank.containsKey(uuid.get().toString())) {
            final double oldBalance = this.bank.get(uuid.get().toString());
            this.bank.replace(uuid.get().toString(), oldBalance + v);
        } else {
            try (final ResultSet resultSet = database.readQuery("SELECT * FROM `economy` WHERE `uuid` = '" + uuid.get().toString() + "';").get()) {
                double result = 0;
                final boolean isSet = resultSet.next();
                if (isSet)
                    result = resultSet.getDouble("money");
                if (Bukkit.getOfflinePlayer(uuid.get()).isOnline())
                    this.bank.put(uuid.get().toString(), result + v);
                else if (isSet)
                    database.modifyQuery("UPDATE `economy` SET `money` = '" + (result + v) + "' WHERE `uuid` = '" + uuid.get().toString() + "'");
                else
                    database.modifyQuery("INSERT INTO `economy` (`uuid`, `money`) VALUES ('" + name + "', '" + (result - v) + "');");
                return null;
            } catch (Exception ignored) {
            }
        }
        return null;
    }

    @Override
    public EconomyResponse depositPlayer(final OfflinePlayer offlinePlayer, final double v) {
        if (this.bank.containsKey(offlinePlayer.getUniqueId().toString())) {
            final double oldBalance = this.bank.get(offlinePlayer.getUniqueId().toString());
            this.bank.replace(offlinePlayer.getUniqueId().toString(), oldBalance + v);
        } else {
            try (final ResultSet resultSet = database.readQuery("SELECT * FROM `economy` WHERE `uuid` = '" + offlinePlayer.getUniqueId().toString() + "';").get()) {
                double result = 0;
                final boolean isSet = resultSet.next();
                if (isSet)
                    result = resultSet.getDouble("money");
                if (offlinePlayer.isOnline())
                    this.bank.put(offlinePlayer.getUniqueId().toString(), result + v);
                else if (isSet)
                    database.modifyQuery("UPDATE `economy` SET `money` = '" + (result + v) + "' WHERE `uuid` = '" + offlinePlayer.getUniqueId().toString() + "'");
                else
                    database.modifyQuery("INSERT INTO `economy` (`uuid`, `money`) VALUES ('" + offlinePlayer.getUniqueId().toString() + "', '" + (result - v) + "');");
                return null;
            } catch (Exception ignored) {
            }
        }
        return null;
    }

    @Override
    public EconomyResponse depositPlayer(final String name, final String s1, final double v) {
        return this.depositPlayer(name, v);
    }

    @Override
    public EconomyResponse depositPlayer(final OfflinePlayer offlinePlayer, final String s, final double v) {
        return depositPlayer(offlinePlayer, v);
    }

    @Override
    public EconomyResponse createBank(String s, String s1) {
        return null;
    }

    @Override
    public EconomyResponse createBank(String s, OfflinePlayer offlinePlayer) {
        return null;
    }

    @Override
    public EconomyResponse deleteBank(String s) {
        return null;
    }

    @Override
    public EconomyResponse bankBalance(String s) {
        return null;
    }

    @Override
    public EconomyResponse bankHas(String s, double v) {
        return null;
    }

    @Override
    public EconomyResponse bankWithdraw(String s, double v) {
        return null;
    }

    @Override
    public EconomyResponse bankDeposit(String s, double v) {
        return null;
    }

    @Override
    public EconomyResponse isBankOwner(String s, String s1) {
        return null;
    }

    @Override
    public EconomyResponse isBankOwner(String s, OfflinePlayer offlinePlayer) {
        return null;
    }

    @Override
    public EconomyResponse isBankMember(String s, String s1) {
        return null;
    }

    @Override
    public EconomyResponse isBankMember(String s, OfflinePlayer offlinePlayer) {
        return null;
    }

    @Override
    public List<String> getBanks() {
        final Map<String, Double> banks = new HashMap<>();
        try (final ResultSet resultSet = database.readQuery("SELECT * FROM `economy`;").get()) {
            while (resultSet.next())
                banks.put(resultSet.getString("uuid"), resultSet.getDouble("money"));
        } catch (Exception ignored) {
        }
        this.bank.forEach(banks::putIfAbsent);
        return banks.keySet().stream().map(name -> {
            try {
                final UUID uuid = UUID.fromString(name);
                return Bukkit.getOfflinePlayer(uuid).getName();
            } catch (Exception ignored) {
            }
            return name;
        }).collect(Collectors.toList());
    }

    @Override
    public boolean createPlayerAccount(String s) {
        return false;
    }

    @Override
    public boolean createPlayerAccount(OfflinePlayer offlinePlayer) {
        return false;
    }

    @Override
    public boolean createPlayerAccount(String s, String s1) {
        return false;
    }

    @Override
    public boolean createPlayerAccount(OfflinePlayer offlinePlayer, String s) {
        return false;
    }
}