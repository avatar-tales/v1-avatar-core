package fr.avatarreturns.avatarcore.integrations.vault;

import fr.avatarreturns.avatarcore.AvatarCorePlugin;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.ServicePriority;

public class VaultHook {

    private static boolean hooked = false;

    public static RegisteredServiceProvider<Economy> hook() {
        Bukkit.getServicesManager().register(Economy.class, EconomyProvider.get(), AvatarCorePlugin.get(), ServicePriority.Normal);
        final RegisteredServiceProvider<Economy> resp = AvatarCorePlugin.get().getServer().getServicesManager().getRegistration(Economy.class);
        hooked = resp != null;
        return resp;
    }

    public static void unhook() {
        Bukkit.getServicesManager().unregister(Economy.class, EconomyProvider.get());
    }

    public static boolean isHooked() {
        return hooked;
    }

}
