package fr.avatarreturns.avatarcore.scoreboard;

import fr.avatarreturns.api.scoreboard.IScoreboardSign;
import net.minecraft.server.v1_16_R3.*;
import org.bukkit.craftbukkit.v1_16_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class ScoreboardSign implements IScoreboardSign {

    private boolean created = false;
    private final IVirtualTeam[] lines = new IVirtualTeam[15];
    private final Player player;
    private String objectiveName;

    public ScoreboardSign(final Player player, final String objectiveName) {
        this.player = player;
        this.objectiveName = objectiveName;
    }

    @Override
    public void create() {
        if (created)
            return;

        PlayerConnection player = getPlayer();
        player.sendPacket(createObjectivePacket(0, objectiveName));
        player.sendPacket(setObjectiveSlot());
        int i = 0;
        while (i < lines.length)
            sendLine(i++);

        created = true;
    }

    @Override
    public void destroy() {
        if (!created)
            return;

        getPlayer().sendPacket(createObjectivePacket(1, null));
        for (IVirtualTeam team : lines)
            if (team != null)
                getPlayer().sendPacket(team.removeTeam());

        created = false;
    }

    @Override
    public void setObjectiveName(final String name) {
        this.objectiveName = name;
        if (created)
            getPlayer().sendPacket(createObjectivePacket(2, name));
    }

    @Override
    public void setLine(final int line, final String value) {
        IVirtualTeam team = getOrCreateTeam(line);
        String old = team.getCurrentPlayer();

        if (old != null && created)
            getPlayer().sendPacket(removeLine(old));

        team.setValue(value);
        sendLine(line);
    }

    @Override
    public void removeLine(final int line) {
        IVirtualTeam team = getOrCreateTeam(line);
        String old = team.getCurrentPlayer();

        if (old != null && created) {
            getPlayer().sendPacket(removeLine(old));
            getPlayer().sendPacket(team.removeTeam());
        }

        lines[line] = null;
    }

    @Override
    public String getLine(final int line) {
        if (line > 14)
            return null;
        if (line < 0)
            return null;
        return getOrCreateTeam(line).getValue();
    }

    @Override
    public IVirtualTeam getTeam(final int line) {
        if (line > 14)
            return null;
        if (line < 0)
            return null;
        return getOrCreateTeam(line);
    }

    private PlayerConnection getPlayer() {
        return ((CraftPlayer) player).getHandle().playerConnection;
    }

    private void sendLine(int line) {
        if (line > 14)
            return;
        if (line < 0)
            return;
        if (!created)
            return;

        int score = (15 - line);
        IVirtualTeam val = getOrCreateTeam(line);
        for (PacketPlayOutScoreboardTeam packet : val.sendLine())
            getPlayer().sendPacket(packet);
        getPlayer().sendPacket(sendScore(val.getCurrentPlayer(), score));
        val.reset();
    }

    private IVirtualTeam getOrCreateTeam(int line) {
        if (lines[line] == null)
            lines[line] = new VirtualTeam("__fakeScore" + line);

        return lines[line];
    }

    private Packet<?> createObjectivePacket(int mode, String displayName) {
        PacketPlayOutScoreboardObjective packet = new PacketPlayOutScoreboardObjective();
        // Nom de l'objectif
        setField(packet, "a", player.getName());

        // Mode
        // 0 : créer
        // 1 : Supprimer
        // 2 : Mettre à jour
        setField(packet, "d", mode);

        if (mode == 0 || mode == 2) {
            setField(packet, "b", IChatBaseComponent.ChatSerializer.a("{\"text\":\"" + displayName + "\"}"));
            setField(packet, "c", IScoreboardCriteria.EnumScoreboardHealthDisplay.INTEGER);
        }

        return packet;
    }

    private Packet<?> setObjectiveSlot() {
        PacketPlayOutScoreboardDisplayObjective packet = new PacketPlayOutScoreboardDisplayObjective();
        // Slot
        setField(packet, "a", 1);
        setField(packet, "b", player.getName());

        return packet;
    }

    private Packet<?> sendScore(String line, int score) {

        return new PacketPlayOutScoreboardScore(ScoreboardServer.Action.CHANGE, player.getName(), line, score);
    }

    private Packet<?> removeLine(String line) {
        return new PacketPlayOutScoreboardScore(ScoreboardServer.Action.REMOVE, player.getName(), line, 0);
    }

    public static class VirtualTeam implements IVirtualTeam {

        private final String name;
        private String prefix;
        private String suffix;
        private String currentPlayer;
        private String oldPlayer;

        private boolean prefixChanged, suffixChanged, playerChanged = false;
        private boolean first = true;

        private VirtualTeam(String name, String prefix, String suffix) {
            this.name = name;
            this.prefix = prefix;
            this.suffix = suffix;
        }

        VirtualTeam(String name) {
            this(name, "", "");
        }

        @Override
        public String getName() {
            return this.name;
        }

        @Override
        public String getPrefix() {
            return this.prefix;
        }

        @Override
        public void setPrefix(final String prefix) {
            if (this.prefix == null || !this.prefix.equals(prefix))
                this.prefixChanged = true;
            this.prefix = prefix;
        }

        @Override
        public String getSuffix() {
            return this.suffix;
        }

        @Override
        public void setSuffix(final String suffix) {
            if (this.suffix == null || !this.suffix.equals(prefix))
                this.suffixChanged = true;
            this.suffix = suffix;
        }

        private PacketPlayOutScoreboardTeam createPacket(int mode) {
            PacketPlayOutScoreboardTeam packet = new PacketPlayOutScoreboardTeam();
            setField(packet, "a", name);
            setField(packet, "b", IChatBaseComponent.ChatSerializer.a("{\"text\":\"" + "" + "\"}"));
            setField(packet, "c", IChatBaseComponent.ChatSerializer.a("{\"text\":\"" + prefix + "\"}"));
            setField(packet, "d", IChatBaseComponent.ChatSerializer.a("{\"text\":\"" + suffix + "\"}"));
            setField(packet, "i", 0);
            setField(packet, "e", "always");
            setField(packet, "g", EnumChatFormat.RESET);
            setField(packet, "i", mode);
            return packet;
        }

        @Override
        public PacketPlayOutScoreboardTeam createTeam() {
            return createPacket(0);
        }

        @Override
        public PacketPlayOutScoreboardTeam updateTeam() {
            return createPacket(2);
        }

        @Override
        public Packet<?> removeTeam() {
            PacketPlayOutScoreboardTeam packet = new PacketPlayOutScoreboardTeam();
            setField(packet, "a", name);
            setField(packet, "i", 1);
            first = true;
            return packet;
        }

        @Override
        public void setPlayer(final String name) {
            if (this.currentPlayer == null || !this.currentPlayer.equals(name))
                this.playerChanged = true;
            this.oldPlayer = this.currentPlayer;
            this.currentPlayer = name;

        }

        @Override
        public Iterable<PacketPlayOutScoreboardTeam> sendLine() {
            List<PacketPlayOutScoreboardTeam> packets = new ArrayList<>();

            if (first) {
                packets.add(createTeam());
            } else if (prefixChanged || suffixChanged) {
                packets.add(updateTeam());
            }

            if (first || playerChanged) {
                if (oldPlayer != null)
                    packets.add(addOrRemovePlayer(4, oldPlayer));
                packets.add(changePlayer());
            }

            if (first)
                first = false;

            return packets;

        }

        @Override
        public void reset() {
            prefixChanged = false;
            suffixChanged = false;
            playerChanged = false;
            oldPlayer = null;
        }

        @Override
        public PacketPlayOutScoreboardTeam changePlayer() {
            return addOrRemovePlayer(3, currentPlayer);
        }

        @Override
        @SuppressWarnings("unchecked")
        public PacketPlayOutScoreboardTeam addOrRemovePlayer(final int mode, final String playerName) {
            PacketPlayOutScoreboardTeam packet = new PacketPlayOutScoreboardTeam();
            setField(packet, "a", name);
            setField(packet, "i", mode);

            try {
                Field f = packet.getClass().getDeclaredField("h");
                f.setAccessible(true);
                ((List<String>) f.get(packet)).add(playerName);
            } catch (NoSuchFieldException | IllegalAccessException e) {
                e.printStackTrace();
            }

            return packet;

        }

        @Override
        public String getCurrentPlayer() {
            return currentPlayer;
        }

        @Override
        public String getValue() {
            return getPrefix() + getCurrentPlayer() + getSuffix();
        }

        @Override
        public void setValue(String value) {
            if (value.length() <= 16) {
                setPrefix("");
                setSuffix("");
                setPlayer(value);
            } else if (value.length() <= 32) {
                setPrefix(value.substring(0, 16));
                setPlayer(value.substring(16));
                setSuffix("");
            } else if (value.length() <= 48) {
                setPrefix(value.substring(0, 16));
                setPlayer(value.substring(16, 32));
                setSuffix(value.substring(32));
            } else {
                throw new IllegalArgumentException("Too long value ! Max 48 characters, value was " + value.length() + " !");
            }
        }
    }

    private static void setField(Object edit, String fieldName, Object value) {
        try {
            Field field = edit.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            field.set(edit, value);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }


}
