package fr.avatarreturns.avatarcore.animators.listener;

import fr.avatarreturns.api.AvatarReturnsAPI;
import fr.avatarreturns.avatarcore.AvatarCorePlugin;
import fr.avatarreturns.avatarcore.animators.Animator;
import fr.avatarreturns.avatarcore.animators.commands.AnimatorExecutor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.EquipmentSlot;

import java.util.logging.Level;

public class AnimatorListening implements Listener {

    public AnimatorListening() {
        AvatarCorePlugin.get().getServer().getPluginManager().registerEvents(this, AvatarCorePlugin.get());
    }

    @EventHandler
    public void onPlayerDrop(final PlayerDropItemEvent e) {
        if (Animator.getAnimator(e.getPlayer()) != null) {
            e.setCancelled(true);
            e.getPlayer().sendMessage("§c§lAnimateur §4§l» §cVous ne pouvez pas drop d'items.");
        }
    }

    @EventHandler
    public void onAsyncPlayerChat(final AsyncPlayerChatEvent e){
        final Animator animator = Animator.getAnimator(e.getPlayer());
        if (animator != null) {
            if(animator.getGiveMod()){
                animator.getGiveParts().add(e.getMessage());
                e.setCancelled(true);
                e.getPlayer().sendMessage("§3§lAnimateur §8§l» §7Morceau de commande ajouté.");
            }
        }
    }

    @EventHandler
    public void onBlockBreak(final BlockBreakEvent e) {
        if (Animator.getAnimator(e.getPlayer()) != null) {
            if (AvatarReturnsAPI.get().isIntegrate("WorldGuard")) {
                if (!Animator.canDoThat(e.getBlock().getLocation(), e.getPlayer())) {
                    e.setCancelled(true);
                    e.getPlayer().sendMessage("§c§lAnimateur §4§l» §cVous ne pouvez pas casser de blocs hors d'une région dont vous êtes le propriétaire.");
                    return;
                }
            }
            e.setDropItems(false);
            AvatarReturnsAPI.get().log(Level.SEVERE, "ANIMATOR " + e.getPlayer().getName() + " BREAK : " + e.getBlock().getType().name() + " AT " + e.getBlock().getLocation());
        }
    }

    @EventHandler
    public void onBlockPlace(final BlockPlaceEvent e) {
        if (Animator.getAnimator(e.getPlayer()) != null) {
            if (AvatarReturnsAPI.get().isIntegrate("WorldGuard")) {
                if (!Animator.canDoThat(e.getBlockPlaced().getLocation(), e.getPlayer())) {
                    e.setCancelled(true);
                    e.getPlayer().sendMessage("§c§lAnimateur §4§l» §cVous ne pouvez pas poser de blocs hors d'une région dont vous êtes le propriétaire.");
                    return;
                }
            }
            AvatarReturnsAPI.get().log(Level.SEVERE, "ANIMATOR " + e.getPlayer().getName() + " PLACE : " + e.getBlockPlaced().getType().name() + " AT " + e.getBlockPlaced().getLocation());
        }
    }

    @EventHandler
    public void onPlayerSwapHandItemsEvent(final PlayerSwapHandItemsEvent e){
        if (Animator.getAnimator(e.getPlayer()) != null) {
            if (e.getPlayer().isSneaking() && e.getOffHandItem() != null){
                if (e.getOffHandItem().getType().equals(Material.STICK) && e.getOffHandItem().getItemMeta().getDisplayName().startsWith("§k§7§l§cCréateur de zones")) {
                    e.setCancelled(true);
                    Animator.getAnimator(e.getPlayer()).switchSelectorMod();
                }
            }
        }
    }

    @EventHandler
    public void onPlayerInteract(final PlayerInteractEvent e) {
        if(e.getHand() == null) return;
        if (!e.getHand().equals(EquipmentSlot.HAND)) return;
        if (e.getPlayer().getInventory().getItemInMainHand() == null) return;
        if (AvatarReturnsAPI.get().isIntegrate("WorldGuard")) {
            final Animator animator = Animator.getAnimator(e.getPlayer());
            if (animator != null) {
                if (e.getPlayer().getInventory().getItemInMainHand().getType().equals(Material.STICK) && e.getPlayer().getInventory().getItemInMainHand().getItemMeta().getDisplayName().startsWith("§k§7§l§cCréateur de zones")) {
                    e.setCancelled(true);
                    if (e.getAction().equals(Action.LEFT_CLICK_BLOCK)) {
                        animator.setStarter(e.getClickedBlock().getLocation());
                        if(animator.getSelectorMod().equals(Animator.SelectorMod.CUBOID))
                            e.getPlayer().sendMessage("§3§lAnimateur §8§l» §7Point 1 placé");
                        else
                            e.getPlayer().sendMessage("§3§lAnimateur §8§l» §7Point de départ placé.");
                        return;
                    }
                    if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
                        if(animator.getSelectorMod().equals(Animator.SelectorMod.CUBOID)) {
                            animator.getPoints().clear();
                            animator.getPoints().add(e.getClickedBlock().getLocation());
                            e.getPlayer().sendMessage("§3§lAnimateur §8§l» §7Point 2 placé");
                        }
                        else {
                            if (animator.getStarter() == null) {
                                e.getPlayer().sendMessage("§c§lAnimateur §4§l» §cVous devez d'abord placé le point de départ.");
                                return;
                            }
                            animator.addPoint(e.getClickedBlock().getLocation());
                            e.getPlayer().sendMessage("§3§lAnimateur §8§l» §7Point ajouté.");
                        }
                        return;
                    }
                }
                if(e.getClickedBlock() != null && e.getAction().equals(Action.RIGHT_CLICK_BLOCK)){
                    if(e.getClickedBlock().getType().equals(Material.ENDER_CHEST)){
                        e.setCancelled(true);
                        e.getPlayer().sendMessage("§c§lAnimateur §4§l» §cVous ne pouvez pas ouvrir d'enderchest.");
                    }
                    if(e.getClickedBlock().getType().toString().contains("SHULKER_BOX")){
                        e.setCancelled(true);
                        e.getPlayer().sendMessage("§c§lAnimateur §4§l» §cVous ne pouvez pas ouvrir de shulker box.");
                    }
                }
            }
        }
    }

    @EventHandler
    public void onPlayerCommandPreprocess(final PlayerCommandPreprocessEvent e) {
        final Animator animator = Animator.getAnimator(e.getPlayer());
        if (AvatarReturnsAPI.get().isIntegrate("WorldGuard")) {
            if (animator != null) {
                if (!Animator.canDoThat(e.getPlayer()) && !e.getMessage().toLowerCase().contains("animateur")) {
                    e.setCancelled(true);
                    e.getPlayer().sendMessage("§c§lAnimateur §4§l» §cVous ne pouvez faire cela que dans une zone dont vous êtes le proprio.");
                    return;
                }
            }
        }
        if (animator != null) {
            if(e.getMessage().toLowerCase().contains("economy") || e.getMessage().toLowerCase().contains("pay")) {
                e.setCancelled(true);
                e.getPlayer().sendMessage("§c§lAnimateur §4§l» §cImpossible d'exécuter cette commande en tant qu'animateur.");
            }
            AvatarReturnsAPI.get().log(Level.SEVERE, "ANIMATOR " + e.getPlayer().getName() + " USE COMMAND : " + e.getMessage());
            return;
        }
        if(e.getMessage().toLowerCase().contains("pay")){
            for(final String str : e.getMessage().split(" ")){
                for(Animator animator1 : Animator.getList()){
                    if(animator1.getPlayer().getName().equalsIgnoreCase(str)){
                        e.setCancelled(true);
                        e.getPlayer().sendMessage("§c§lAvatar Security §4§l» §cImpossible d'exécuter cette commande en vers un animateur.");
                        return;
                    }
                }
            }
        }
    }

    @EventHandler
    public void onPlayerQuit(final PlayerQuitEvent e) {
        if (Animator.getAnimator(e.getPlayer()) != null) {
            Animator.getAnimator(e.getPlayer()).destroy();
        }
        AnimatorExecutor.remove(e.getPlayer());
    }


}
