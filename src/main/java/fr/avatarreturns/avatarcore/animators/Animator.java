package fr.avatarreturns.avatarcore.animators;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.math.BlockVector2;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.flags.Flags;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedPolygonalRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.sk89q.worldguard.protection.regions.RegionContainer;
import fr.avatarreturns.api.AvatarReturnsAPI;
import fr.avatarreturns.avatarcore.animators.commands.AnimatorExecutor;
import fr.avatarreturns.avatarcore.animators.listener.AnimatorListening;
import net.luckperms.api.LuckPermsProvider;
import net.luckperms.api.node.Node;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

public class Animator {

    private static List<Animator> animators;
    private final Player player;
    private final List<Location> points;
    private double balance;
    private boolean givemod;
    private List<String> giveParts;
    private SelectorMod selectorMod;
    private Location starter;
    private ProtectedRegion region;

    public Animator(final Player player) {
        this.player = player;
        this.points = new ArrayList<>();
        this.givemod = false;
        this.giveParts = new ArrayList<>();
        this.init();
    }

    public static void create() {
        animators = new ArrayList<>();
        new AnimatorExecutor();
        new AnimatorListening();
    }

    public static Animator getAnimator(final Player player) {
        for (final Animator animator : Animator.animators) {
            if (animator.player.equals(player))
                return animator;
        }
        return null;
    }

    public static boolean canDoThat(final Player player) {
        return Animator.canDoThat(player.getLocation(), player);
    }

    public static boolean canDoThat(final Location location, final Player player) {
        final RegionContainer regionContainer = WorldGuard.getInstance().getPlatform().getRegionContainer();
        final RegionManager regionManager = regionContainer.get(BukkitAdapter.adapt(location.getWorld()));
        for (final ProtectedRegion region : regionManager.getApplicableRegions(BlockVector3.at(location.getX(), location.getY(), location.getZ()))) {
            if (region.getOwners().contains(player.getUniqueId()))
                return true;
        }
        return false;
    }

    public static ProtectedRegion getRegionByInsideOwner(final Player player) {
        final Location location = player.getLocation();
        final RegionContainer regionContainer = WorldGuard.getInstance().getPlatform().getRegionContainer();
        final RegionManager regionManager = regionContainer.get(BukkitAdapter.adapt(location.getWorld()));
        for (final ProtectedRegion region : regionManager.getApplicableRegions(BlockVector3.at(location.getX(), location.getY(), location.getZ()))) {
            if (region.getOwners().contains(player.getUniqueId()))
                return region;
        }
        return null;
    }

    public static ProtectedRegion getRegion(final String name, final Player player) {
        final Location location = player.getLocation();
        final RegionContainer regionContainer = WorldGuard.getInstance().getPlatform().getRegionContainer();
        final RegionManager regionManager = regionContainer.get(BukkitAdapter.adapt(location.getWorld()));
        for (final ProtectedRegion region : regionManager.getApplicableRegions(BlockVector3.at(location.getX(), location.getY(), location.getZ()))) {
            if (region.getId().equalsIgnoreCase(name))
                return region;
        }
        return null;
    }

    public void init() {
        Animator.animators.add(this);
        AvatarReturnsAPI.get().log(Level.INFO, "[ANIMATOR MOD] " + this.player.getName() + " ENABLED");
        this.balance = AvatarReturnsAPI.get().getUserWithoutLoadIt(this.player.getUniqueId()).getMoney();
        this.player.closeInventory();
        this.player.getInventory().clear();
        this.player.setGameMode(GameMode.CREATIVE);
        this.selectorMod = SelectorMod.CUBOID;
        this.player.getInventory().addItem(AvatarReturnsAPI.get().getUtils().getItem(Material.STICK, "&k&7&l&cCréateur de zones " + this.selectorMod.getName()));
        /*for (final String permission : AvatarRP.getFileManager().getFile("animators").getStringList("permissions")) {
            LuckPermsProvider.get().getUserManager().getUser(this.player.getUniqueId()).data().add(Node.builder(permission).build());
        }*/
    }

    public void destroy() {
        this.player.closeInventory();
        this.player.getInventory().clear();
        this.player.setGameMode(GameMode.SURVIVAL);
        if(this.region != null)
            this.region.getOwners().removePlayer(this.player.getUniqueId());
        /*for (final String permission : AvatarRP.getFileManager().getFile("animators").getStringList("permissions")) {
            LuckPermsProvider.get().getUserManager().getUser(this.player.getUniqueId()).data().remove(Node.builder(permission).build());
        }*/
        AvatarReturnsAPI.get().getUserWithoutLoadIt(this.player.getUniqueId()).removeMoney(AvatarReturnsAPI.get().getUserWithoutLoadIt(this.player.getUniqueId()).getMoney());
        AvatarReturnsAPI.get().getUserWithoutLoadIt(this.player.getUniqueId()).addMoney(this.balance);
        this.balance = 0;
        AvatarReturnsAPI.get().log(Level.INFO, "[ANIMATOR MOD] " + this.player.getName() + " DISABLED");
        Animator.animators.remove(this);
        try {
            this.finalize();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    public void addPoint(final Location point) {
        this.points.add(point);
    }

    public void createRegion(final String name) throws Exception {
        final List<BlockVector2> blockVector2s = new ArrayList<>();
        blockVector2s.add(BlockVector2.at(this.starter.getX(), this.starter.getZ()));
        for (final Location location : this.points)
            blockVector2s.add(BlockVector2.at(location.getX(), location.getZ()));
        ProtectedRegion region = null;
        if(this.selectorMod.equals(SelectorMod.CUBOID))
            region = new ProtectedCuboidRegion(name, BlockVector3.at(this.starter.getX(), 0, this.starter.getZ()), BlockVector3.at(this.points.get(0).getX(), 255, this.points.get(0).getZ()));
        else if(this.selectorMod.equals(SelectorMod.POLYGON))
            region = new ProtectedPolygonalRegion(name, blockVector2s, 0, 255);
        assert region != null;
        final RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(this.starter.getWorld()));
        if(region.getIntersectingRegions(regionManager.getRegions().values()).size() >= 1){
            throw new Exception();
        }
        region.getOwners().addPlayer(this.player.getUniqueId());
        region.setFlag(Flags.ENTRY, StateFlag.State.DENY);
        region.setFlag(Flags.MOB_SPAWNING, StateFlag.State.DENY);
        region.setFlag(Flags.USE, StateFlag.State.ALLOW);
        region.setFlag(Flags.INTERACT, StateFlag.State.ALLOW);
        region.setFlag(Flags.CHEST_ACCESS, StateFlag.State.DENY);
        region.setFlag(Flags.EXIT_OVERRIDE, true);
        region.setFlag(Flags.EXIT_VIA_TELEPORT, StateFlag.State.DENY);
        regionManager.addRegion(region);
        this.setEditRegion(region);
    }

    public void switchSelectorMod(){
        this.selectorMod = SelectorMod.fromValue(this.selectorMod.getValue() + 1);
        this.starter = null;
        this.getPoints().clear();
        for(final ItemStack itemStack : this.player.getInventory().getContents()){
            if(itemStack == null) continue;
            if(itemStack.getItemMeta() == null) continue;
            if(itemStack.getType().equals(Material.STICK) && itemStack.getItemMeta().getDisplayName().startsWith("§k§7§l§cCréateur de zones")){
                final ItemMeta itemMeta = itemStack.getItemMeta();
                itemMeta.setDisplayName("§k§7§l§cCréateur de zones " + this.selectorMod.getName());
                itemStack.setItemMeta(itemMeta);
            }
        }
    }

    public SelectorMod getSelectorMod() {
        return this.selectorMod;
    }

    public ProtectedRegion getEditRegion() {
        return this.region;
    }

    public Player getPlayer(){
        return this.player;
    }

    public boolean getGiveMod(){
        return this.givemod;
    }
    public boolean setGiveMod(final boolean givemod){
        this.givemod = givemod;
        return this.givemod;
    }

    public List<String> getGiveParts() {
        return giveParts;
    }

    public void setEditRegion(final ProtectedRegion region) {
        if(this.region != null){
            this.region.getOwners().removePlayer(this.player.getUniqueId());
        }
        this.region = region;
        if(region != null)
            if(!region.getOwners().contains(this.player.getUniqueId()))
                region.getOwners().addPlayer(this.player.getUniqueId());
    }

    public Location getStarter() {
        return this.starter;
    }

    public void setStarter(final Location starter) {
        this.starter = starter;
        this.points.clear();
    }

    public List<Location> getPoints() {
        return this.points;
    }

    public enum SelectorMod{
        CUBOID("§7Cuboïde", 0),
        POLYGON("§7Polygone", 1);

        private final String name;
        private final int value;

        SelectorMod(final String displayName, final int value){
            this.name = displayName;
            this.value = value;
        }

        public String getName() {
            return this.name;
        }

        public int getValue() {
            return this.value;
        }

        public static SelectorMod fromValue(final int value){
            for(final SelectorMod selectorMod : SelectorMod.values()){
                if(selectorMod.getValue() == value % SelectorMod.values().length)
                    return selectorMod;
            }
            return null;
        }
    }

    public static List<Animator> getList(){
        return Animator.animators;
    }

}

