package fr.avatarreturns.avatarcore.animators.commands;

import com.projectkorra.projectkorra.util.Utils;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.flags.Flags;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.sk89q.worldguard.protection.regions.RegionContainer;
import fr.avatarreturns.api.AvatarReturnsAPI;
import fr.avatarreturns.api.commands.ICommandHandler;
import fr.avatarreturns.api.commands.RegisterCommand;
import fr.avatarreturns.avatarcore.AvatarCorePlugin;
import fr.avatarreturns.avatarcore.animators.Animator;
import fr.avatarreturns.avatarcore.permissions.Permissions;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Location;
import org.bukkit.NamespacedKey;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AnimatorExecutor implements ICommandHandler {

    private static final List<Player> toConfirm;

    static {
        toConfirm = new ArrayList<>();
    }

    public AnimatorExecutor() {
        AvatarReturnsAPI.get().registerCommandClass(this);
    }

    public static void remove(final Player player) {
        AnimatorExecutor.toConfirm.remove(player);
    }

    @RegisterCommand(command = "/animateur", permission = "avatar.animateur")
    public void run(final CommandSender commandSender, final List<String> args) {
        if (!(commandSender instanceof Player)) {
            commandSender.sendMessage("§c§lAnimateur §4§l» §cSeul un joueur peut exécuter cette commande.");
            return;
        }
        final Player player = (Player) commandSender;
        if (!player.hasPermission(Permissions.ANIMATOR.get())) {
            player.sendMessage("§c§lAnimateur §4§l» §cVous n'êtes pas abilité à faire cette commande.");
            return;
        }
        if (args.size() > 0) {
            final Animator animator = Animator.getAnimator(player);
            if (args.get(0).equalsIgnoreCase("on")) {
                if (animator != null) {
                    player.sendMessage("§c§lAnimateur §4§l» §cVous êtes déjà en mode animateur.");
                    return;
                }
                player.sendMessage("§3§lAnimateur §c§l» §7Vous devez confirmer votre passage en mode animateur via la commande §c/animator confirm§7. §4⚠ §cVous allez être clear en entrant en mode animateur. §4⚠");
                if (!AnimatorExecutor.toConfirm.contains(player)) AnimatorExecutor.toConfirm.add(player);
                return;
            }
            if (args.get(0).equalsIgnoreCase("off")) {
                if (animator == null) {
                    player.sendMessage("§c§lAnimateur §4§l» §cVous n'êtes pas en mode animateur.");
                    return;
                }
                animator.destroy();
                player.sendMessage("§3§lAnimateur §8§l» §7Vous avez quitté le mode animateur.");
                return;
            }
            if (args.get(0).equalsIgnoreCase("confirm")) {
                if (!AnimatorExecutor.toConfirm.contains(player)) {
                    player.sendMessage("§c§lAnimateur §4§l» §cVous êtes déjà en mode animateur.");
                    return;
                }
                new Animator(player);
                AnimatorExecutor.toConfirm.remove(player);
                player.sendMessage("§3§lAnimateur §8§l» §7Vous êtes passé en mode animateur.");
                return;
            }
                /*
            if (args.get(0).equalsIgnoreCase("region") && AvatarReturnsAPI.get().isIntegrate("WorldGuard")) {
                if (args.size() < 2) {
                    player.sendMessage("§c§lAnimateur §4§l» §cTrop peu d'arguments.");
                    return;
                }
                if (animator == null && !player.isOp()) {
                    player.sendMessage("§c§lAnimateur §4§l» §cVous n'êtes pas en mode animateur.");
                    return;
                }
                if (args.get(1).equalsIgnoreCase("create") && animator != null) {
                    if (args.size() < 3) {
                        player.sendMessage("§c§lAnimateur §4§l» §cTrop peu d'arguments.");
                        return;
                    }
                    if (animator.getStarter() == null || animator.getPoints().size() == 0) {
                        player.sendMessage("§c§lAnimateur §4§l» §cVous devez faire une sélection.");
                        return;
                    }
                    try {
                        animator.createRegion(args.get(2));
                        player.sendMessage("§3§lAnimateur §8§l» §7Zone §b" + args.get(2) + " §7créée. Vous éditez maintenant cette zone.");
                        return;
                    }
                    catch (final Exception ignored){
                        player.sendMessage("§c§lAnimateur §4§l» §cImpossible de créer cette zone. Elle empiète peut-être sur une autre.");
                        return;
                    }
                }
                if (args.get(1).equalsIgnoreCase("edit") && animator != null) {
                    if (args.length < 3) {
                        player.sendMessage("§c§lAnimateur §4§l» §cTrop peu d'arguments.");
                        return;
                    }
                    final ProtectedRegion region = Animator.getRegion(args[2], player);
                    if (region == null) {
                        player.sendMessage("§c§lAnimateur §4§l» §cCette zone n'existe pas.");
                        return;
                    }
                    if(AvatarRP.getFileManager().getFile("animators").getStringList("blacklist").contains(region.getId())){
                        player.sendMessage("§c§lAnimateur §4§l» §cCette zone est bloquée, vous ne pouvez pas la modifier.");
                        return;
                    }
                    animator.setEditRegion(region);
                    player.sendMessage("§3§lAnimateur §8§l» §7Zone §b" + args.get(2) + " §7éditée.");
                    return;
                }
                if (args.get(1).equalsIgnoreCase("setteleport") && animator != null) {
                    final ProtectedRegion region = animator.getEditRegion();
                    if (region == null) {
                        player.sendMessage("§c§lAnimateur §4§l» §cVous n'êtes pas en train d'éditer une zone.");
                        return;
                    }
                    if (!region.contains(BlockVector3.at(player.getLocation().getX(), player.getLocation().getY(), player.getLocation().getZ()))) {
                        player.sendMessage("§c§lAnimateur §4§l» §cVous n'êtes pas dans la zone.");
                        return;
                    }
                    region.getFlags().put(Flags.TELE_LOC, BukkitAdapter.adapt(player.getLocation()));
                    player.sendMessage("§3§lAnimateur §8§l» §7Point de téléportation de la zone §b" + region.getId() + " §7mis à jour.");
                    return;
                }
                if (args.get(1.equalsIgnoreCase("delete")) {
                    final ProtectedRegion region = animator.getEditRegion();
                    if (region == null) {
                        player.sendMessage("§c§lAnimateur §4§l» §cVous n'êtes pas en train d'éditer une zone.");
                        return;
                    }
                    final RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(player.getWorld()));
                    regionManager.removeRegion(region.getId());
                    player.sendMessage("§3§lAnimateur §8§l» §7Région §b" + region.getId() + " §7supprimée.");
                    return;
                }
                if (args.get(1).equalsIgnoreCase("teleport") && animator != null) {
                    final ProtectedRegion region = animator.getEditRegion();
                    if (region == null) {
                        player.sendMessage("§c§lAnimateur §4§l» §cVous n'êtes pas en train d'éditer une zone.");
                        return;
                    }
                    player.teleport(BukkitAdapter.adapt(Objects.requireNonNull(region.getFlag(Flags.TELE_LOC))));
                    player.sendMessage("§3§lAnimateur §8§l» §7Point de téléportation de la zone §b" + region.getId() + " §7mis à jour.");
                    return;
                }
            }*/
            if (args.get(0).equalsIgnoreCase("setlore")) {
                if (animator == null && !player.isOp()) {
                    player.sendMessage("§c§lAnimateur §4§l» §cVous n'êtes pas en mode animateur.");
                    return;
                }
                try {
                    final StringBuilder stringBuilder = new StringBuilder();
                    for (int i = 1; i < args.size(); i++) {
                        stringBuilder.append(ChatColor.translateAlternateColorCodes('&', args.get(i)));
                        if (i != args.size() - 1)
                            stringBuilder.append(" ");
                    }
                    final List<String> lore = new ArrayList<>(Arrays.asList(stringBuilder.toString().split("%nl%")));
                    AvatarReturnsAPI.get().getUtils().modifyItem(player.getInventory().getItemInMainHand(), lore);
                    return;
                } catch (Exception e) {
                    player.sendMessage("§c§lAnimateur §4§l» §cVous devez avoir un item dans votre main principal.");
                    return;
                }
            }
            // TODO: REPLACE OLD SYSTEM
            /*
            if (args[0].equalsIgnoreCase("applyskin")) {
                if (args.length == 1) {
                    player.sendMessage("§c§lAnimateur §4§l» §cIl faut renseigner un id !");
                    return;
                }
                if (args.length == 2) {
                    player.sendMessage("§c§lAnimateur §4§l» §cVous devez renseigner le nom du skin !");
                    return;
                }
                if (!NumberUtils.isNumber(args.get(1))) {
                    player.sendMessage("§c§lAnimateur §4§l» §cL'id doit être un nombre !");
                    return;
                }
                final int id = Integer.parseInt(args.get(1));
                final NPC npc = CitizensAPI.getNPCRegistry().getById(id);
                if (npc == null) {
                    player.sendMessage("§c§lAnimateur §4§l» §cNPC inexistant !");
                    return;
                }
                final FileConfiguration config = AvatarRP.getFileManager().getFile("skins");
                final ConfigurationSection section = config.getConfigurationSection("skins");
                if (section == null) {
                    player.sendMessage("§c§lAnimateur §4§l» §cImpossible de récupérer les données.");
                    return;
                }
                final boolean isSet = config.get("skins." + args.get(1)) != null;
                if (!isSet) {
                    player.sendMessage("§c§lAnimateur §4§l» §cSkin introuvable.");
                    return;
                }
                final String value = config.getString("skins." + args[2] + ".value");
                final String signature = config.getString("skins." + args[2] + ".signature");
                if (!npc.hasTrait(SkinTrait.class))
                    npc.addTrait(SkinTrait.class);
                npc.getTrait(SkinTrait.class).setSkinPersistent(args[2], signature, value);
            }if (args[0].equalsIgnoreCase("setskin")) {
                if (args.length == 1) {
                    player.sendMessage("§c§lAnimateur §4§l» §cVous devez renseigner le nom du skin !");
                    return;
                }
                final String[] textures = Utils.getSkin(player, null);
                final boolean alreadySet = AvatarRP.getFileManager().getFile("skins").get("skins." + args.get(1)) != null;
                AvatarRP.getFileManager().setLine("skins","skins." + args.get(1) + ".value", textures[0]);
                AvatarRP.getFileManager().setLine("skins","skins." + args.get(1) + ".signature", textures[1]);
                if (alreadySet)
                    player.sendMessage("§c§lAnimateur §4§l» §cSkin précédent écrasé.");
                player.sendMessage("§3§lAnimateur §8§l» §7Skin ajouté.");
            }
            if (args[0].equalsIgnoreCase("listskin")) {
                final FileConfiguration config = AvatarRP.getFileManager().getFile("skins");
                final ConfigurationSection section = config.getConfigurationSection("skins");
                if (section == null || section.getKeys(false).size() == 0) {
                    player.sendMessage("§c§lAnimateur §4§l» §cIl n'y a aucun skin.");
                    return;
                }
                player.sendMessage("§3§lAnimateur §8§l» §7Liste des skins : ");
                for (final String name : section.getKeys(false)) {
                    player.sendMessage("§6§l-|> §7" + name);
                }
            }
            if (args[0].equalsIgnoreCase("delskin")) {
                if (args.length == 1) {
                    player.sendMessage("§c§lAnimateur §4§l» §cVous devez renseigner le nom du skin !");
                    return;
                }
                final boolean isSet = AvatarRP.getFileManager().getFile("skins").get("skins." + args.get(1)) != null;
                if (!isSet) {
                    player.sendMessage("§c§lAnimateur §4§l» §cSkin introuvable.");
                    return;
                }
                AvatarRP.getFileManager().setLine("skins","skins." + args.get(1), null);
                player.sendMessage("§3§lAnimateur §8§l» §7Skin supprimé.");
            }*/
            if (args.get(0).equalsIgnoreCase("give")) {
                if (animator == null) {
                    player.sendMessage("§c§lAnimateur §4§l» §cVous n'êtes pas en mode animateur.");
                    return;
                }
                try {
                    if(animator.getGiveMod()) {
                        final StringBuilder command = new StringBuilder();
                        for(final String part : animator.getGiveParts()){
                            command.append(part);
                        }
                        AvatarCorePlugin.get().getServer().dispatchCommand(AvatarCorePlugin.get().getServer().getConsoleSender(), "minecraft:give " + player.getName() + " " + command.toString() + " 1");
                        player.sendMessage("§3§lAnimateur §8§l» §7Objet donné.");
                        animator.getGiveParts().clear();
                        animator.setGiveMod(false);
                        return;
                    }
                    player.sendMessage("§3§lAnimateur §8§l» §7Écrivez dans le chat l'objet que vous voulez (e.g. 'minecraft:player_head{SkullOwner:\"" + player.getName() + "\"}'). Puis réexécutez cette commande pour avoir l'item.");
                    animator.getGiveParts().clear();
                    animator.setGiveMod(true);
                    return;
                } catch (Exception e) {
                    player.sendMessage("§c§lAnimateur §4§l» §cVous devez avoir un item dans votre main principal.");
                    return;
                }
            }
            if (args.get(0).equalsIgnoreCase("setenchant")) {
                if (animator == null && !player.isOp()) {
                    player.sendMessage("§c§lAnimateur §4§l» §cVous n'êtes pas en mode animateur.");
                    return;
                }
                try {
                    final ItemStack itemStack = player.getInventory().getItemInMainHand();
                    for (final String enchants : args.get(1).split(";")) {
                        Enchantment enchantment;
                        int level;
                        try {
                            enchantment = Enchantment.getByKey(NamespacedKey.minecraft(enchants.split(",")[0]));
                        } catch (Exception e) {
                            player.sendMessage("§c§lAnimateur §4§l» §cCet enchantement n'existe pas.");
                            return;
                        }
                        try {
                            level = Integer.parseInt(enchants.split(",")[1]);
                        } catch (Exception e) {
                            player.sendMessage("§c§lAnimateur §4§l» §cLe niveau doit être un entier.");
                            return;
                        }
                        assert enchantment != null;
                        itemStack.addUnsafeEnchantment(enchantment, level);
                    }
                    return;
                } catch (Exception e) {
                    player.sendMessage("§c§lAnimateur §4§l» §cVous devez avoir un item dans votre main principal.");
                    return;
                }
            }
            if (args.get(0).equalsIgnoreCase("hideflags")) {
                if (animator == null && !player.isOp()) {
                    player.sendMessage("§c§lAnimateur §4§l» §cVous n'êtes pas en mode animateur.");
                    return;
                }
                try {
                    final ItemStack itemStack = player.getInventory().getItemInMainHand();
                    final ItemMeta itemMeta = itemStack.getItemMeta();
                    try {
                        if (itemMeta.getItemFlags().contains(ItemFlag.valueOf(args.get(1).toUpperCase())))
                            itemMeta.removeItemFlags(ItemFlag.valueOf(args.get(1).toUpperCase()));
                        else
                            itemMeta.addItemFlags(ItemFlag.valueOf(args.get(1).toUpperCase()));
                    }
                    catch (Exception ignored){
                        player.sendMessage("§c§lAnimateur §4§l» §cFlag invalide.");
                        return;
                    }
                    itemStack.setItemMeta(itemMeta);
                    return;
                } catch (Exception e) {
                    player.sendMessage("§c§lAnimateur §4§l» §cVous devez avoir un item dans votre main principal.");
                    return;
                }
            }
            if (args.get(0).equalsIgnoreCase("setname")) {
                if (animator == null && !player.isOp()) {
                    player.sendMessage("§c§lAnimateur §4§l» §cVous n'êtes pas en mode animateur.");
                    return;
                }
                try {
                    final StringBuilder stringBuilder = new StringBuilder();
                    for (int i = 1; i < args.size(); i++) {
                        stringBuilder.append(ChatColor.translateAlternateColorCodes('&', args.get(i)));
                        if (i != args.size() - 1)
                            stringBuilder.append(" ");
                    }
                    AvatarReturnsAPI.get().getUtils().modifyItem(player.getInventory().getItemInMainHand(), stringBuilder.toString());
                    return;
                } catch (Exception e) {
                    player.sendMessage("§c§lAnimateur §4§l» §cVous devez avoir un item dans votre main principal.");
                    return;
                }
            }

            if (args.get(0).equalsIgnoreCase("skull")) {
                if (animator == null) {
                    player.sendMessage("§c§lAnimateur §4§l» §cVous n'êtes pas en mode animateur.");
                    return;
                }
                try {
                    AvatarReturnsAPI.get().getUtils().getSkull(args.get(1)).ifPresent(
                        player.getInventory()::addItem
                    );
                    return;
                } catch (Exception e) {
                    player.sendMessage("§c§lAnimateur §4§l» §cImpossible de récupérer la tête.");
                    return;
                }
            }
        }
        return;

    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String alias, @NotNull String[] args) {
        if (!(sender instanceof Player) || !sender.hasPermission(Permissions.ANIMATOR.get())) {
            return null;
        }
        final Player player = (Player) sender;
        final List<String> completions = new ArrayList<>();
        final Animator animator = Animator.getAnimator(player);
        if (animator == null && !player.isOp()) {
            if (args.length == 1 && !AnimatorExecutor.toConfirm.contains(player) && "on".startsWith(args[0].toLowerCase())) {
                completions.add("on");
            }
            if (args.length == 1 && AnimatorExecutor.toConfirm.contains(player) && "confirm".startsWith(args[0].toLowerCase())) {
                completions.add("confirm");
            }
            return completions;
        }
        if (args.length == 1) {
            final String[] list = {"hideflags", "off", "region", "setenchant", "setlore", "setname", "skull"};
            for (final String part : list) {
                if (part.startsWith(args[0].toLowerCase()) && (!(part.equals("off") || part.equals("region")) || animator != null))
                    completions.add(part);
            }
            return completions;
        }
        if (args.length == 2 && args[0].equalsIgnoreCase("hideflags")){
            for (final ItemFlag itemFlag : ItemFlag.values()) {
                if (itemFlag.name().toLowerCase().startsWith(args[1].toLowerCase()))
                    completions.add(itemFlag.name());
            }
            return completions;
        }
        if (args.length >= 2 && args[0].equalsIgnoreCase("region") && animator != null) {
            if (args.length == 3 && args[1].equalsIgnoreCase("edit")) {
                final Location location = player.getLocation();
                final RegionContainer regionContainer = WorldGuard.getInstance().getPlatform().getRegionContainer();
                final RegionManager regionManager = regionContainer.get(BukkitAdapter.adapt(location.getWorld()));
                for (final ProtectedRegion region : regionManager.getApplicableRegions(BlockVector3.at(location.getX(), location.getY(), location.getZ()))) {
                    if (region.getId().toLowerCase().startsWith(args[2].toLowerCase()))
                        completions.add(region.getId());
                }
                return completions;
            }
            final String[] list = {"create", "edit", "setteleport", "teleport"};
            for (final String part : list) {
                if (part.startsWith(args[1].toLowerCase()))
                    completions.add(part);
            }
            return completions;
        }
        return null;

    }
}
