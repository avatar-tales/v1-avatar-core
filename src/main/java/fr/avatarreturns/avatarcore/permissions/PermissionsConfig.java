package fr.avatarreturns.avatarcore.permissions;

import fr.avatarreturns.api.AvatarReturnsAPI;
import fr.avatarreturns.api.storage.files.IFileManager;
import fr.avatarreturns.avatarcore.AvatarCore;

import java.util.Optional;

public class PermissionsConfig {

    public static void init() {
        final Optional<IFileManager.IConfig> config = AvatarReturnsAPI.get().getFileManager().createFile(AvatarCore.get().getPlugin().getDataFolder().getAbsolutePath(), "permissions.yml");
        config.ifPresent(iConfig -> {
                for (final Permissions permissions : Permissions.values()) {
                    if (iConfig.getConfig().get(permissions.name().replace("_", ".")) == null)
                        iConfig.getConfig().set(permissions.name().replace("_", "."), permissions.permission);
                    permissions.setPermission(iConfig.getConfig().getString(permissions.name().replace("_", ".")));
                }
        });
    }
}
