package fr.avatarreturns.avatarcore.permissions;

public enum Permissions {

    ANIMATOR("avatar.animator"),

    DICE("avatar.dice.cheat"),

    FACTION_SPY("avatar.factionchat.spy"),

    SPAWN("avatar.spawn"),
    SPAWN_SET("avatar.spawn.set"),
    SPAWN_QUICK("avatar.spawn.quick");

    String permission;

    Permissions(String permission) {
        this.permission = permission;
    }

    public String get() {
        return permission;
    }

    public void setPermission(final String permission) {
        this.permission = permission;
    }
}
