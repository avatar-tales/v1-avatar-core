package fr.avatarreturns.avatarcore.inventory;

import fr.avatarreturns.api.AvatarReturnsAPI;
import fr.avatarreturns.api.schedulers.IUpdated;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

public class Scheduler implements IUpdated {

    private static Scheduler instance;

    public static Scheduler getInstance() {
        if (instance == null)
            instance = new Scheduler();
        return instance;
    }

    private final List<Inventory> inventories = new ArrayList<>();

    private int step;

    private Scheduler() {
        this.step = 0;
    }

    @Override
    public void run() {
        if (step++ % 2 == 0)
            inventories.forEach(inventoryAPI -> {
                inventoryAPI.getInventory().clear();
                if (inventoryAPI.getFunction() != null)
                    inventoryAPI.getFunction().accept(inventoryAPI);
                inventoryAPI.getItems().forEach(itemAPI -> inventoryAPI.getInventory().setItem(itemAPI.getSlot(), itemAPI.getItem()));
            });
        else
            inventories.forEach(inventoryAPI -> inventoryAPI.getItems().forEach(itemAPI -> itemAPI.refresh(inventoryAPI)));
    }

    private void start() {
        this.step = 0;
        AvatarReturnsAPI.get().registerUpdated(this);
    }

    public void stop(final JavaPlugin plugin) {
        this.stop();
    }

    private void stop() {
        AvatarReturnsAPI.get().unregisterUpdated(this);
    }

    synchronized void add(final Inventory inv) {
        this.inventories.add(inv);
        if (this.inventories.size() == 1)
            start();
    }

    synchronized void remove(final Inventory inv) {
        this.inventories.remove(inv);
        if (this.inventories.size() == 0)
            stop();
    }

}
