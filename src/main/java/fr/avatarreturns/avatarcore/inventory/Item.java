package fr.avatarreturns.avatarcore.inventory;

import fr.avatarreturns.api.AvatarReturnsAPI;
import fr.avatarreturns.api.inventory.ItemAPI;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.function.Consumer;
import java.util.function.Function;

public class Item implements ItemAPI {

    private final int slot;
    private final Function<Object, ItemStack> function;
    private ItemStack item;
    private final boolean cancelled;
    private final Consumer<InventoryClickEvent> consumer;

    public Item(final int slot, final ItemStack item, final boolean cancelled, final Consumer<InventoryClickEvent> consumer) {
        this.slot = slot;
        this.item = item;
        this.function = null;
        this.cancelled = cancelled;
        this.consumer = consumer;
    }

    public Item(final int slot, final Function<Object, ItemStack> function, final boolean cancelled, final Consumer<InventoryClickEvent> consumer) {
        this.slot = slot;
        this.function = function;
        this.item = new ItemStack(Material.AIR);
        this.refresh(this);
        this.cancelled = cancelled;
        this.consumer = consumer;
    }

    @Override
    public void refresh(final Object o) {
        if (this.function == null)
            return;
        this.item = this.function.apply(o);
    }

    @Override
    public int getSlot() {
        return this.slot;
    }

    @Override
    public ItemStack getItem() {
        if (this.item == null)
            this.refresh(this);
        if (this.item == null)
            return AvatarReturnsAPI.get().getUtils().getItem(Material.AIR);
        return this.item;
    }

    @Override
    public boolean isCancelled() {
        return this.cancelled;
    }

    @Override
    public Consumer<InventoryClickEvent> getConsumer() {
        return this.consumer;
    }
}
