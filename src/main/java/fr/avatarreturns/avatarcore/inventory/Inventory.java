package fr.avatarreturns.avatarcore.inventory;

import fr.avatarreturns.api.inventory.InventoryAPI;
import fr.avatarreturns.api.inventory.ItemAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

public class Inventory implements InventoryAPI, Listener {

    private org.bukkit.inventory.Inventory inventory;
    private int size;
    private String title;
    private List<ItemAPI> items;
    private Consumer<InventoryAPI> function;
    private boolean refreshed;

    private JavaPlugin plugin;

    private Inventory(final JavaPlugin plugin) {
        this.plugin = plugin;
        this.size = 9;
        this.title = "";
        this.items = new ArrayList<>();
        this.refreshed = false;
    }

    private Inventory() {
    }

    public static InventoryAPI create(final JavaPlugin plugin) {
        return new Inventory(plugin);
    }

    @Override
    public InventoryAPI setSize(final int size) {
        if (this.size <= 0 || this.size % 9 != 0 || this.size >= 54) {
            plugin.getLogger().severe("This inventory can't have a size of " + size);
            return this;
        }
        if (this.inventory != null && this.size != size) {
            this.inventory.clear();
            this.inventory = Bukkit.createInventory(null, size, this.title);
        }
        this.size = size;
        return this;
    }

    @Override
    public InventoryAPI setTitle(final String title) {
        if (this.inventory != null && !this.title.equals(title)) {
            this.inventory.clear();
            this.inventory = Bukkit.createInventory(null, this.size, title);
        }
        this.title = title;
        return this;
    }

    @Override
    public InventoryAPI setRefresh(final boolean refreshed) {
        this.refreshed = refreshed;
        return this;
    }

    @Override
    public InventoryAPI setFunction(Consumer<InventoryAPI> function) {
        this.function = function;
        return this;
    }

    @Override
    public int getSize() {
        return this.size;
    }

    @Override
    public String getTitle() {
        return this.title;
    }

    @Override
    public List<ItemAPI> getItems() {
        return this.items;
    }

    @Override
    public boolean isRefreshed() {
        return this.refreshed;
    }

    JavaPlugin getPlugin() {
        return this.plugin;
    }

    org.bukkit.inventory.Inventory getInventory() {
        return this.inventory;
    }

    @Override
    public Consumer<InventoryAPI> getFunction() {
        return function;
    }

    @Override
    public InventoryAPI clearSlot(final int slot) {
        Optional<ItemAPI> itemAPI = Optional.empty();
        for (final Iterator<ItemAPI> it = this.items.iterator(); it.hasNext(); itemAPI = Optional.ofNullable(it.next())) {
            itemAPI.ifPresent(item -> {
                if (item.getSlot() == slot)
                    it.remove();
            });
        }
        return this;
    }

    @Override
    public Optional<ItemAPI> getItem(final int slot) {
        return this.items.stream().filter(item -> item.getSlot() == slot).findFirst();
    }

    @Override
    public InventoryAPI addItem(final int slot, final ItemStack itemStack) {
        return this.addItem(slot, itemStack, true);
    }

    @Override
    public InventoryAPI addItem(final int slot, final Function<Object, ItemStack> function) {
        return this.addItem(slot, function, true);
    }

    @Override
    public InventoryAPI addItem(final int slot, final ItemStack itemStack, final boolean cancelled) {
        return this.addItem(slot, itemStack, cancelled, inventoryClickEvent -> {
        });
    }

    @Override
    public InventoryAPI addItem(final int slot, final Function<Object, ItemStack> function, final boolean cancelled) {
        return this.addItem(slot, function, cancelled, inventoryClickEvent -> {
        });
    }

    @Override
    public InventoryAPI addItem(final int slot, final ItemStack itemStack, final boolean cancelled, final Consumer<InventoryClickEvent> consumer) {
        return this.addItem(new Item(slot, itemStack, cancelled, consumer));
    }

    @Override
    public InventoryAPI addItem(final int slot, final Function<Object, ItemStack> function, final boolean cancelled, final Consumer<InventoryClickEvent> consumer) {
        return this.addItem(new Item(slot, function, cancelled, consumer));
    }

    @Override
    public InventoryAPI addItem(final ItemAPI itemAPI) {
        this.clearSlot(itemAPI.getSlot());
        this.items.add(itemAPI);
        return this;
    }

    @Override
    public void build(final Player player) {
        if (this.inventory == null) {
            this.inventory = Bukkit.createInventory(player, this.size, this.title);
            if (this.function != null)
                this.function.accept(this);
            this.items.forEach(itemAPI -> {
                if (this.inventory.getSize() <= itemAPI.getSlot())
                    return;
                itemAPI.refresh(this);
                this.inventory.setItem(itemAPI.getSlot(), itemAPI.getItem());
            });
            player.openInventory(this.inventory);
            Scheduler.getInstance().add(this);
            plugin.getServer().getPluginManager().registerEvents(this, this.plugin);
        }
        else {
            if (this.function != null)
                this.function.accept(this);
            this.items.forEach(itemAPI -> {
                if (this.inventory.getSize() <= itemAPI.getSlot())
                    return;
                itemAPI.refresh(this);
                this.inventory.setItem(itemAPI.getSlot(), itemAPI.getItem());
            });
        }
    }

    @Override
    public void stop() {
        HandlerList.unregisterAll(this);
        Scheduler.getInstance().remove(this);
        this.inventory = null;
    }

    @EventHandler
    public void onClose(final InventoryCloseEvent e) {
        if (e.getView().getTopInventory().equals(this.inventory))
            this.stop();
        if (!e.getInventory().equals(this.inventory))
            return;
        if (e.getInventory().getHolder() == null) {
            this.stop();
        }
    }

    @EventHandler
    public void onInteract(final InventoryClickEvent e) {
        if (e.getClickedInventory() == null)
            return;
        if (!e.getClickedInventory().equals(this.inventory))
            return;
        this.items.forEach(itemAPI -> {
            if (e.getSlot() != itemAPI.getSlot())
                return;
            if (e.getCurrentItem() == null)
                return;
            e.setCancelled(itemAPI.isCancelled());
            itemAPI.getConsumer().accept(e);
        });
    }
}
