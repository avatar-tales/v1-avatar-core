package fr.avatarreturns.avatarcore.commands;

import fr.avatarreturns.api.commands.ICommandHandler;
import fr.avatarreturns.api.commands.RegisterCommand;
import fr.avatarreturns.avatarcore.permissions.Permissions;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Random;

public class Dice implements ICommandHandler {

    @RegisterCommand(command = "/dice", executorType = RegisterCommand.ExecutorType.PLAYERS)
    public void run(final CommandSender sender, final String[] args) {
        int random = new Random().nextInt(100);
        if (sender.hasPermission(Permissions.DICE.get()) && args.length >= 1) {
            try {
                random = Integer.parseInt(args[0]);
            }
            catch (Exception ignored) {
            }
        }
        ((Player) sender).chat("§6Résultat de mon dice : §e§l" + random);
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender commandSender, @NotNull Command command, @NotNull String s, @NotNull String[] strings) {
        return null;
    }
}
