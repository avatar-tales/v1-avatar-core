package fr.avatarreturns.avatarcore.commands;

import fr.avatarreturns.api.AvatarReturnsAPI;
import fr.avatarreturns.api.commands.ICommandHandler;
import fr.avatarreturns.api.commands.RegisterCommand;
import fr.avatarreturns.api.users.IUser;
import fr.avatarreturns.avatarcore.messages.Messages;
import fr.avatarreturns.avatarcore.permissions.Permissions;
import fr.avatarreturns.avatarcore.storage.files.GeneralConfig;
import fr.avatarreturns.avatarcore.utils.SpawnTeleportation;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Optional;

public class Spawn implements ICommandHandler {

    @RegisterCommand(command = "/spawn", usage = "/spawn [(here | world,x,y,z[,pitch,yaw])]")
    public void spawn(final CommandSender sender, final String[] args) {
        if (sender instanceof Player) {
            if (args.length >= 1 && sender.hasPermission(Permissions.SPAWN_SET.get())) {
                if (args[0].equals("here")) {
                    GeneralConfig.setSpawnLocation(((Player) sender).getLocation());
                    sender.sendMessage(Messages.INFO_SPAWN_UPDATE.get());
                    return;
                }
                else {
                    try {
                        setSpawnFromLocationString(args);
                        sender.sendMessage(Messages.INFO_SPAWN_UPDATE.get());
                    }
                    catch (Exception e) {
                        sender.sendMessage(AvatarReturnsAPI.get().getMessages().getReflectionError());
                        e.printStackTrace();
                    }
                }
                return;
            }
            if (GeneralConfig.getSpawnLocation() == null) {
                sender.sendMessage(Messages.ERROR_SPAWN_NULL.get());
                return;
            }
            if (!sender.hasPermission(Permissions.SPAWN.get()) && false) {
                sender.sendMessage(AvatarReturnsAPI.get().getMessages().getNoPermission());
                return;
            }
            if (GeneralConfig.getSpawnBlackListWorld().contains(((Player) sender).getWorld().getName())) {
                sender.sendMessage(Messages.ERROR_SPAWN_CANCEL.get());
                return;
            }

            new SpawnTeleportation((Player) sender);
            return;

        }
        else {
            if (args.length == 0) {
                sender.sendMessage(AvatarReturnsAPI.get().getMessages().getNeedPlayer());
            }
            else if (args.length >= 4) {
                try {
                    setSpawnFromLocationString(args);
                    sender.sendMessage(Messages.INFO_SPAWN_UPDATE.get());
                }
                catch (Exception e) {
                    sender.sendMessage(AvatarReturnsAPI.get().getMessages().getReflectionError());
                    e.printStackTrace();
                }
            }
            else {
                sender.sendMessage(AvatarReturnsAPI.get().getMessages().getNotEnoughArguments());
            }
        }
    }

    private void setSpawnFromLocationString(final String[] args) {
        final String[] params = args[0].split(",");
        final World world = Bukkit.getServer().getWorld(params[0]);
        final double
                x = Double.parseDouble(params[1]),
                y = Double.parseDouble(params[2]),
                z = Double.parseDouble(params[3]);
        float pitch = 0, yaw = 0;
        if (args.length > 4)
            pitch = Float.parseFloat(args[4]);
        if (args.length > 5)
            yaw = Float.parseFloat(args[5]);
        GeneralConfig.setSpawnLocation(new Location(world, x, y, z, pitch, yaw));
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender commandSender, @NotNull Command command, @NotNull String s, @NotNull String[] strings) {
        return null;
    }
}
