package fr.avatarreturns.avatarcore.commands;

import fr.avatarreturns.api.AvatarReturnsAPI;
import fr.avatarreturns.api.commands.ICommandHandler;
import fr.avatarreturns.api.commands.RegisterCommand;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.InvocationTargetException;
import java.time.Instant;
import java.util.Calendar;
import java.util.List;

public class Lag implements ICommandHandler {

    final long startedAt;

    public Lag() {
        this.startedAt = Instant.now().getEpochSecond();
    }

    @RegisterCommand(command = "/lag")
    public void lag(final CommandSender sender, final String[] args) throws NoSuchMethodException {
        sender.sendMessage("§e§l§m+------------------------------------------+");
        sender.sendMessage("");
        sender.sendMessage("           §c§lINFORMATIONS SERVEUR");
        sender.sendMessage("");
        sender.sendMessage("§cDate §7§o➫ §e" + getDate(Calendar.getInstance()));
        sender.sendMessage("§cTPS (5s, 1m, 5m, 15m) §7§o➫ §e" + getTPS());
        if(sender instanceof Player)
            sender.sendMessage("§cPing §7§o➫ §e" + getPing((Player) sender) + "ms");
        else
            sender.sendMessage("§cPing §7§o➫ §e0ms §cBAH OUI VIVE LE LOCAL");
        sender.sendMessage("§cUptime §7§o➫ §e" + getUptime());
        if(sender instanceof Player) {
            if(args.length == 0) {
                sender.sendMessage("§cVotre monde §7:");
                final World world = ((Player) sender).getWorld();
                sender.sendMessage("    §7§o➫ §6" + world.getName() + "§7: §e" + world.getLoadedChunks().length + " §6chunks§7, §e" + world.getEntities().size() + " §6entités");
            }
            else if (args[0].equalsIgnoreCase("all")) {
                sender.sendMessage("§cMondes §7:");
                for (final World world : AvatarReturnsAPI.get().getPlugin().getServer().getWorlds()) {
                    sender.sendMessage("    §7§o➫ §6" + world.getName() + "§7: §e" + world.getLoadedChunks().length + " §6chunks§7, §e" + world.getEntities().size() + " §6entités");
                }
            }
            else {
                showAllWorlds(sender, args);
            }
        }
        else {
            if (args.length == 0 || args[0].equalsIgnoreCase("all")) {
                sender.sendMessage("§cMondes §7:");
                for (final World world : AvatarReturnsAPI.get().getPlugin().getServer().getWorlds()) {
                    sender.sendMessage("    §7§o➫ §6" + world.getName() + "§7: §e" + world.getLoadedChunks().length + " §6chunks§7, §e" + world.getEntities().size() + " §6entités");
                }
            }
            else {
                showAllWorlds(sender, args);
            }
        }
        sender.sendMessage("");
        sender.sendMessage("§e§l§m+------------------------------------------+");
    }

    private void showAllWorlds(CommandSender sender, String[] args) {
        for (final World world : AvatarReturnsAPI.get().getPlugin().getServer().getWorlds()) {
            if (world.getName().equalsIgnoreCase(args[0])) {
                sender.sendMessage("§cMonde §7:");
                sender.sendMessage("    §7§o➫ §6" + world.getName() + "§7: §e" + world.getLoadedChunks().length + " §6chunks§7, §e" + world.getEntities().size() + " §6entités");
            }
        }
    }

    private static String getDate(final Calendar calendar) {
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minutes = calendar.get(Calendar.MINUTE);
        int seconds = calendar.get(Calendar.SECOND);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH) + 1;
        int year = calendar.get(Calendar.YEAR);
        return (hour < 10 ? "0" : "") + hour + ":" +
                (minutes < 10 ? "0" : "") + minutes + ":" +
                (seconds < 10 ? "0" : "") + seconds + " " +
                (day < 10 ? "0" : "") + day + "/" +
                (month < 10 ? "0" : "") + month + "/" +
                year;
    }

    private String getUptime() {
        final long duration = Instant.now().getEpochSecond() - this.startedAt;
        final int hours = (int) duration / 3600;
        final int minutes = (int) (duration % 3600) / 60;
        final int seconds = (int) duration % 60;
        if (hours > 0)
            return String.format("%02dh %02dm %02ds", hours, minutes, seconds);
        if (minutes > 0)
            return String.format("%02dm %02ds", minutes, seconds);
        if (seconds > 0)
            return String.format("%02ds", seconds);
        return "à l'instant";
    }

    public static int getPing(final Player player) {
        try {
            final Object craftPlayer = Class.forName("org.bukkit.craftbukkit.v1_16_R3.entity.CraftPlayer")
                    .getMethod("getHandle")
                    .invoke(player);
            return (int) craftPlayer.getClass().getField("ping").get(craftPlayer);
        } catch (IllegalArgumentException |
                IllegalAccessException |
                NoSuchFieldException |
                SecurityException |
                ClassNotFoundException |
                NoSuchMethodException |
                InvocationTargetException e) {
            return 0;
        }
    }

    private static String getTPS() {
        try {
            final Object craftServer = Class.forName("org.bukkit.craftbukkit.v1_16_R3.CraftServer")
                    .getMethod("getServer")
                    .invoke(AvatarReturnsAPI.get().getPlugin().getServer());
            final StringBuilder stringBuilder = new StringBuilder();
            for (final double tps : (double[]) craftServer.getClass().getField("recentTps").get(craftServer)) {
                stringBuilder.append(tps >= 20 ? (tps > 20 ? "20*" : "20") : (double) ((int) Math.floor(tps*1000))/1000);
                stringBuilder.append(" ");
            }
            return stringBuilder.toString();
        } catch (IllegalArgumentException |
                IllegalAccessException |
                NoSuchFieldException |
                SecurityException |
                ClassNotFoundException |
                NoSuchMethodException |
                InvocationTargetException e) {
            return "0, 0, 0";
        }
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender commandSender, @NotNull Command command, @NotNull String s, @NotNull String[] strings) {
        return null;
    }
}
