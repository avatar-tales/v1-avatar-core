package fr.avatarreturns.avatarcore.commands.votifier;

import fr.avatarreturns.api.commands.ICommandHandler;
import fr.avatarreturns.api.commands.RegisterCommand;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class Vote implements ICommandHandler {

    @RegisterCommand(command = "/vote", executorType = RegisterCommand.ExecutorType.PLAYERS)
    public void run(final CommandSender sender, final String[] args) {
        TextComponent message = new TextComponent("§bCliquez ici pour aller voter");
        message.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "http://vote.avatar-returns.fr"));
        message.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Clique ici pour voter !").create()));
        sender.spigot().sendMessage(message);
        return;
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender commandSender, @NotNull Command command, @NotNull String s, @NotNull String[] strings) {
        return null;
    }
}
