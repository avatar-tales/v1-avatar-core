package fr.avatarreturns.avatarcore.commands;

import fr.avatarreturns.api.commands.ICommandHandler;
import fr.avatarreturns.api.commands.RegisterCommand;
import fr.avatarreturns.avatarcore.chat.Chat;
import fr.avatarreturns.avatarcore.utils.Utils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Random;

public class LocalSpy implements ICommandHandler {

    @RegisterCommand(command = "/localspy", executorType = RegisterCommand.ExecutorType.PLAYERS)
    public void run(final CommandSender sender, final String[] args) {

        int random = new Random().nextInt(100);

        if (!Utils.havePermission(((Player) sender),"avatar.spychat",true))return;

        if(Chat.lstSpyChatUser.contains(sender)){
            sender.sendMessage("§c§lSpyChat §6OFF");
            Chat.lstSpyChatUser.remove((Player)sender);
        }
        else{
            sender.sendMessage("§c§lSpyChat §aON");
            Chat.lstSpyChatUser.add((Player)sender);
        }
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender commandSender, @NotNull Command command, @NotNull String s, @NotNull String[] strings) {
        return null;
    }
}
