package fr.avatarreturns.avatarcore.commands;

import fr.avatarreturns.api.AvatarReturnsAPI;
import fr.avatarreturns.api.commands.ICommandHandler;
import fr.avatarreturns.api.commands.RegisterCommand;
import fr.avatarreturns.avatarcore.AvatarCore;
import fr.avatarreturns.avatarcore.AvatarCorePlugin;
import org.bukkit.Bukkit;
import org.bukkit.command.*;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.SimplePluginManager;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

public class CommandHandler implements CommandExecutor {

    private final Plugin plugin;
    private final Map<String, RegisteredCommand> registeredCommandTable = new HashMap<>();

    private CommandFailureHandler failureHandler = (sender, reason, command) -> {};

    public interface CommandFailureHandler {
        void handleFailure(CommandFailReason reason, CommandSender sender, RegisteredCommand command);
    }

    enum CommandFailReason {
        INSUFFICIENT_PARAMETER,
        REDUNDANT_PARAMETER,
        NO_PERMISSION,
        NOT_PLAYER,
        NOT_CONSOLE,
        COMMAND_NOT_FOUND,
        REFLECTION_ERROR
    }

    private static CommandHandler instance;

    public static CommandHandler getInstance(final Plugin plugin) {
        if (instance == null)
            instance = new CommandHandler(plugin);
        return instance;
    }

    private CommandHandler() {
        this.plugin = null;
    }

    private CommandHandler(Plugin plugin) {
        this.plugin = plugin;
        this.setCustomFailureHandler((reason, sender, command) -> {
            switch (reason) {
                case INSUFFICIENT_PARAMETER:
                    sender.sendMessage(AvatarReturnsAPI.get().getMessages().getNotEnoughArguments());
                    break;
                case REDUNDANT_PARAMETER:
                    sender.sendMessage("§c§lAvatarSecurity §4§» §cVous devez renseigner moins de paramètres.");
                    break;
                case NO_PERMISSION:
                    sender.sendMessage(AvatarReturnsAPI.get().getMessages().getNoPermission());
                    break;
                case NOT_PLAYER:
                    sender.sendMessage(AvatarReturnsAPI.get().getMessages().getNeedPlayer());
                    break;
                case NOT_CONSOLE:
                    sender.sendMessage(AvatarReturnsAPI.get().getMessages().getNeedConsole());
                    break;
                case COMMAND_NOT_FOUND:
                    sender.sendMessage(AvatarReturnsAPI.get().getMessages().getCommandNotFound());
                    break;
                case REFLECTION_ERROR:
                    sender.sendMessage(AvatarReturnsAPI.get().getMessages().getReflectionError());
                    break;
            }
        });
    }

    public void setCustomFailureHandler(final CommandFailureHandler failureHandler) {
        this.failureHandler = failureHandler;
    }

    public void registerCommands(final ICommandHandler object) {
        for (Method method : object.getClass().getMethods()) {
            final RegisterCommand annotation = method.getAnnotation(RegisterCommand.class);

            if (annotation != null) {
                final String base = annotation.command().split(" ")[0].substring(1);
                PluginCommand basePluginCommand = plugin.getServer().getPluginCommand(base);

                if (basePluginCommand == null) {
                    try {
                        final Constructor<PluginCommand> c = PluginCommand.class.getDeclaredConstructor(String.class, Plugin.class);
                        c.setAccessible(true);
                        basePluginCommand = c.newInstance(base, AvatarCorePlugin.get());
                        basePluginCommand.setDescription(annotation.description());
                        basePluginCommand.setLabel(base);
                        basePluginCommand.setPermission(annotation.permission());
                        basePluginCommand.setUsage(annotation.usage());
                        final SimplePluginManager manager = ((SimplePluginManager) Bukkit.getPluginManager());
                        final Field field = manager.getClass().getDeclaredField("commandMap");
                        field.setAccessible(true);
                        ((SimpleCommandMap) field.get(manager)).registerAll(AvatarCorePlugin.get().getName(), Collections.singletonList(basePluginCommand));
                    } catch (Exception e) {
                        throw new RuntimeException(String.format("Unable to register command base '%s'. Did you put it in plugin.yml?", base));
                    }
                }
                basePluginCommand.setExecutor(this);
                basePluginCommand.setTabCompleter(object);
                registeredCommandTable.put(annotation.command().substring(1), new RegisteredCommand(method, object, annotation));
            }
        }
    }

    public void unregisterCommands(final ICommandHandler object) {
        final List<Map.Entry<String, RegisteredCommand>> commands = new ArrayList<>();
        for (final Map.Entry<String, RegisteredCommand> command : this.registeredCommandTable.entrySet()) {
            for (final Method method : object.getClass().getMethods()) {
                if (command.getValue().method.equals(method))
                    commands.add(command);
            }
        }
        commands.forEach((commandEntry) -> this.registeredCommandTable.remove(commandEntry.getKey(), commandEntry.getValue()));
    }

    @Override
    public boolean onCommand(final @NotNull CommandSender sender, final @NotNull Command command, final @NotNull String label, final String[] args) {

        final StringBuilder sb = new StringBuilder();
        for (int i = -1; i <= args.length - 1; i++) {
            if (i == -1)
                sb.append(command.getName().toLowerCase());
            else
                sb.append(" ").append(args[i].toLowerCase());

            for (final String usage : registeredCommandTable.keySet()) {
                if (usage.equals(sb.toString())) {

                    final RegisteredCommand wrapper = registeredCommandTable.get(usage);
                    final RegisterCommand annotation = wrapper.annotation;
                    final String[] actualParams = Arrays.copyOfRange(args, annotation.command().split(" ").length - 1, args.length);

                    if (!(sender instanceof Player) && annotation.executorType().equals(RegisterCommand.ExecutorType.PLAYERS)) {
                        failureHandler.handleFailure(CommandFailReason.NOT_PLAYER, sender, wrapper);
                        return true;
                    }
                    else if ((sender instanceof Player) && annotation.executorType().equals(RegisterCommand.ExecutorType.CONSOLE)) {
                        failureHandler.handleFailure(CommandFailReason.NOT_CONSOLE, sender, wrapper);
                        return true;
                    }

                    if (!annotation.permission().equals("") && !sender.hasPermission(annotation.permission())) {
                        failureHandler.handleFailure(CommandFailReason.NO_PERMISSION, sender, wrapper);
                        return true;
                    }

                    if (actualParams.length < annotation.parametersMin()) {
                        /*if (actualParams.length > annotation.parameters())
                            failureHandler.handleFailure(CommandFailReason.REDUNDANT_PARAMETER, sender, wrapper);*/
                        failureHandler.handleFailure(CommandFailReason.INSUFFICIENT_PARAMETER, sender, wrapper);
                        return true;
                    }

                    try {
                        wrapper.method.invoke(wrapper.instance, sender, actualParams);
                        return true;
                    }
                    catch (ArrayIndexOutOfBoundsException e) {
                        failureHandler.handleFailure(CommandFailReason.INSUFFICIENT_PARAMETER, sender, wrapper);
                        e.printStackTrace();
                    }
                    catch (IllegalAccessException | InvocationTargetException e) {
                        failureHandler.handleFailure(CommandFailReason.REFLECTION_ERROR, sender, wrapper);
                        e.printStackTrace();
                    }
                    return true;
                }
            }
        }

        failureHandler.handleFailure(CommandFailReason.COMMAND_NOT_FOUND, sender, null);
        return true;
    }

    final static class RegisteredCommand {

        private final ICommandHandler instance;

        private final Method method;
        private final RegisterCommand annotation;

        RegisteredCommand(final Method method, final ICommandHandler instance, final RegisterCommand annotation) {
            this.method = method;
            this.instance = instance;
            this.annotation = annotation;
        }
    }

}
