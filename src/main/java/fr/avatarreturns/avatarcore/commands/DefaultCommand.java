package fr.avatarreturns.avatarcore.commands;

import fr.avatarreturns.api.AvatarReturnsAPI;
import fr.avatarreturns.api.commands.IDefaultCommand;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

public class DefaultCommand extends IDefaultCommand {

    private static DefaultCommand instance;

    public static DefaultCommand getInstance() {
        if (instance == null)
            instance = new DefaultCommand();
        return instance;
    }

    private DefaultCommand(){
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        sender.sendMessage(AvatarReturnsAPI.get().getMessages().getCommandDisabled());
        return false;
    }

}
