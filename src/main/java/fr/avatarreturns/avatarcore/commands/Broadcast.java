package fr.avatarreturns.avatarcore.commands;

import fr.avatarreturns.api.AvatarReturnsAPI;
import fr.avatarreturns.api.commands.ICommandHandler;
import fr.avatarreturns.api.commands.RegisterCommand;
import fr.avatarreturns.avatarcore.messages.Messages;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class Broadcast implements ICommandHandler {

    @RegisterCommand(command = "/broadcast", parametersMin = 1, permission = "avatar.broadcast", usage = "/broadcast <message>")
    public void run(final CommandSender sender, final String[] args) {
        if (args.length == 0) {
            return;
        }
        final StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < args.length; i++) {
            stringBuilder.append(args[i]);
            if (i < args.length - 1)
                stringBuilder.append(" ");
        }
        AvatarReturnsAPI.get().getPlugin().getServer().broadcastMessage(Messages.INFO_BROADCAST.get(stringBuilder.toString()));
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender commandSender, @NotNull Command command, @NotNull String s, @NotNull String[] strings) {
        return null;
    }
}
