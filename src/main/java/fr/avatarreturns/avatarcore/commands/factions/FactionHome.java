package fr.avatarreturns.avatarcore.commands.factions;

import com.massivecraft.factions.cmd.type.TypeWarp;
import com.massivecraft.factions.entity.*;
import com.massivecraft.factions.event.EventFactionsWarpTeleport;
import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.mixin.MixinMessage;
import com.massivecraft.massivecore.mixin.MixinTeleport;
import com.massivecraft.massivecore.mixin.TeleporterException;
import com.massivecraft.massivecore.teleport.Destination;
import com.massivecraft.massivecore.teleport.DestinationSimple;
import com.massivecraft.massivecore.util.Txt;
import fr.avatarreturns.api.commands.ICommandHandler;
import fr.avatarreturns.api.commands.RegisterCommand;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class FactionHome implements ICommandHandler {

    @RegisterCommand(command = "/fhome", executorType = RegisterCommand.ExecutorType.PLAYERS)
    public void run(final CommandSender sender, final String[] args) {

        Player player = (Player) sender;
        MPlayer mPlayer = MPlayer.get(player);

        Faction faction = mPlayer.getFaction();

        if (args.length == 1) {
            faction = FactionColl.get().getByName(args[0]);
        }

        if (faction == null) {
            sender.sendMessage("§cCette faction n'existe pas.");
            return;
        }

        if ((faction.getWarps().isEmpty())) {
            sender.sendMessage("§4Vous n'avez pas de home");
            return;
        }
        Warp warp;
        try {
            warp = TypeWarp.get(faction).read("fhome", sender);
        } catch (MassiveException e) {
            sender.sendMessage("Vous n'avez pas de f home, contactez un administrateur");
            return;
        }

        // Must be valid
        if (!warp.verifyIsValid()) return;

        // Any and MPerm
        if (!MPerm.getPermWarp().has(mPlayer, faction, true)) return;

        if (!MConf.get().warpsTeleportAllowedFromEnemyTerritory && mPlayer.isInEnemyTerritory()) {
            player.sendMessage("Vous ne pouvez pas vous téléporter depuis un territoire ennemie");
        }

        if (!MConf.get().warpsTeleportAllowedFromDifferentWorld && !player.getWorld().getName().equalsIgnoreCase(warp.getWorld())) {
            player.sendMessage("Vous ne pouvez pas vous téléporter depuis ce monde");
        }


        // Event
        EventFactionsWarpTeleport event = new EventFactionsWarpTeleport(player, warp);
        event.run();
        if (event.isCancelled()) return;

        // Apply
        try {
            String warpDesc = Txt.parse("<h>%s <i>in <reset>%s<i>", warp.getName(), faction.describeTo(mPlayer, false));
            Destination destination = new DestinationSimple(warp.getLocation(), warpDesc);
            MixinTeleport.get().teleport(player, destination, player);
        } catch (TeleporterException e) {
            String message = e.getMessage();
            MixinMessage.get().messageOne(player, message);
        }


        return;

    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender commandSender, @NotNull Command command, @NotNull String s, @NotNull String[] strings) {
        return null;
    }
}
