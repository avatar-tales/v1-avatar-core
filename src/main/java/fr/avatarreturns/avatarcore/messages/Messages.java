package fr.avatarreturns.avatarcore.messages;

import org.bukkit.ChatColor;

public enum Messages {

    PREFIX_INFO("&3&lTortues Lions &8&l»&r"),
    PREFIX_ERROR("&c&lAvatarSecurity &4&l»&r"),

    INFO_BROADCAST("&c[&7Broadcast&c] &7{0}"),

    INFO_MONEY("%prefix_info% &7Votre bourse est de {0}Y"),

    INFO_FACTION_CHAT_ALLY("&a[{0}] [{1}] &f{2}{3} &f: {4}"),
    INFO_FACTION_SPY_ALLY("&c[ALLY] [{0}] [{1}] &c{2}{3} &c: {4}"),

    INFO_FACTION_CHAT_FACTION("&2[{0}] [{1}] &f{2}{3} &f: {4}"),
    INFO_FACTION_SPY_FACTION("&c[{0}] [{1}] &c{2}{3} &c: {4}"),

    INFO_FACTION_CHAT_TRUCE("&3[{0}] [{1}] &f{2}{3} &f: {4}"),
    INFO_FACTION_SPY_TRUCE("&c[TRUCE] [{0}] [{1}] &c{2}{3} &c: {4}"),

    INFO_FACTION_CHAT_MODIFIED("%prefix_info% &7Chat modifié en : {0}."),

    INFO_SPAWN_UPDATE("%prefix_info% &7Spawn mis à jour."),
    INFO_SPAWN_TELEPORT("%prefix_info% &7Vous avez été téléporté au spawn"),

    ERROR_FACTION_CHAT_NOTINFACTION("%prefix_error% &cVous n'êtes dans aucune faction."),

    ERROR_MONEY("%prefix_error% &cVous n'avez pas assez d'argent."),

    ERROR_SPAWN_CANCEL_ON_MOVED("%prefix_error% &cVous avez bougé, Téléportation annulé"),
    ERROR_SPAWN_CANCEL("%prefix_error% &cVous ne pouvez pas aller au spawn."),
    ERROR_SPAWN_NULL("%prefix_error% &cAucun spawn trouvé."),

    ERROR_PERMISSION("%prefix_error% &cVous n'avez pas la permission de faire cela."),
    ERROR_COMMAND_ERROR("%prefix_error% &cCommande inconnue."),
    ERROR_COMMAND_ARGUMENT("%prefix_error% &cArgument(s) incorrect(s)."),
    ERROR_COMMAND_DISABLED("%prefix_error% &cCette commande est désactivée."),
    ERROR_UNKNOWN("%prefix_error% &cErreur inconnue. Veuillez contacter un administrateur.");

    String message;

    Messages(String message) {
        this.message = message;
    }

    public String get(final String... arguments) {
        String finalMessage = message.replace("%prefix_info%", Messages.PREFIX_INFO.message)
                .replace("%prefix_error%", Messages.PREFIX_ERROR.message);
        for (int i = 0; i < arguments.length; i++) {
            finalMessage = finalMessage.replace("{" + i + "}", arguments[i]);
        }
        return ChatColor.translateAlternateColorCodes('&', finalMessage);
    }

    void setMessage(final String message) {
        this.message = message;
    }
}
