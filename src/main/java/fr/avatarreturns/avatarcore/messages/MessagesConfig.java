package fr.avatarreturns.avatarcore.messages;

import fr.avatarreturns.api.AvatarReturnsAPI;
import fr.avatarreturns.api.storage.files.IFileManager;
import fr.avatarreturns.avatarcore.AvatarCore;

import java.util.Optional;

public class MessagesConfig {

    public static void init() {
        final Optional<IFileManager.IConfig> config = AvatarReturnsAPI.get().getFileManager().createFile(AvatarCore.get().getPlugin().getDataFolder().getAbsolutePath(), "messages.yml");
        config.ifPresent(iConfig -> {
            for (final Messages messages : Messages.values()) {
                if (iConfig.getConfig().get(messages.name().replace("_", ".")) == null)
                    iConfig.getConfig().set(messages.name().replace("_", "."), messages.message);
                messages.setMessage(iConfig.getConfig().getString(messages.name().replace("_", ".")));
            }
        });
    }
}
