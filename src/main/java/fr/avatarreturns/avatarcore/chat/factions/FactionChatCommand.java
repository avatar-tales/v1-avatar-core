package fr.avatarreturns.avatarcore.chat.factions;

import fr.avatarreturns.api.commands.ICommandHandler;
import fr.avatarreturns.api.commands.RegisterCommand;
import fr.avatarreturns.avatarcore.messages.Messages;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class FactionChatCommand implements ICommandHandler {

    private static FactionChatCommand instance;

    public static FactionChatCommand getInstance() {
        if (instance == null)
            return instance = new FactionChatCommand();
        return instance;
    }

    private FactionChatCommand() {
    }

    @RegisterCommand(command = "/fc", parametersMin = 1, executorType = RegisterCommand.ExecutorType.PLAYERS)
    public void run(final CommandSender sender, final String[] args) {
        FactionChat.Type type;
        switch (args[0].toLowerCase()) {
            case "a":
            case "ally":
                type = FactionChat.Type.ALLY;
                break;
            case "f":
            case "faction":
                type = FactionChat.Type.FACTION;
                break;
            case "t":
            case "truce":
                type = FactionChat.Type.TRUCE;
                break;
            default:
                type = FactionChat.Type.PUBLIC;
                break;
        }
        sender.sendMessage(Messages.INFO_FACTION_CHAT_MODIFIED.get(type.name().toLowerCase()));
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender commandSender, @NotNull Command command, @NotNull String s, @NotNull String[] strings) {
        return null;
    }
}
