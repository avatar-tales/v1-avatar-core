package fr.avatarreturns.avatarcore.chat.factions;

import fr.avatarreturns.api.AvatarReturnsAPI;
import fr.avatarreturns.api.commands.ICommandHandler;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class FactionChat implements ICommandHandler {

    private static final Map<Player, Type> chatMode = new HashMap<>();

    public abstract void sendMessage(final Player player, final String message);

    public static boolean isPublicChat(final Player player) {
        AvatarReturnsAPI.get().debug(player.getName() + " chat :");
        AvatarReturnsAPI.get().debug(" -> " + getChatMode(player));
        return getChatMode(player).clazz == null;
    }

    public static Type getChatMode(final Player player) {
        AvatarReturnsAPI.get().debug(player.getName() + " chat :");
        if (!chatMode.containsKey(player)) {
            chatMode.put(player, Type.PUBLIC);
            AvatarReturnsAPI.get().debug(" -> added");
            return Type.PUBLIC;
        }
        AvatarReturnsAPI.get().debug(" -> " + (chatMode.get(player)));
        return chatMode.get(player);
    }

    public enum Type {

        PUBLIC(null),
        ALLY(FactionAlly.getInstance()),
        TRUCE(FactionTruce.getInstance()),
        FACTION(FactionFaction.getInstance());

        final FactionChat clazz;

        Type(final FactionChat clazz) {
            this.clazz = clazz;
        }

        public FactionChat getClazz() {
            return clazz;
        }
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender commandSender, @NotNull Command command, @NotNull String s, @NotNull String[] strings) {
        return null;
    }

}
