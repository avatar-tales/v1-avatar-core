package fr.avatarreturns.avatarcore.chat.factions;

import fr.avatarreturns.api.AvatarReturnsAPI;
import fr.avatarreturns.api.commands.RegisterCommand;
import fr.avatarreturns.api.users.IUser;
import fr.avatarreturns.avatarcore.integrations.FactionIntegration;
import fr.avatarreturns.avatarcore.messages.Messages;
import fr.avatarreturns.avatarcore.permissions.Permissions;
import fr.avatarreturns.avatarcore.utils.Utils;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Optional;

public class FactionFaction extends FactionChat {

    private static FactionFaction instance;

    public static FactionFaction getInstance() {
        if (instance == null)
            return instance = new FactionFaction();
        return instance;
    }

    private FactionFaction() {
    }

    @Override
    public void sendMessage(final Player player, final String message) {
        final Optional<IUser> iUser = AvatarReturnsAPI.get().getUser(player.getUniqueId());
        iUser.ifPresent(user -> {
            final Optional<String> factionName = user.getFactionName();
            factionName.ifPresent(faction -> {
                if (faction.equalsIgnoreCase("Wild")) {
                    player.sendMessage(Messages.ERROR_FACTION_CHAT_NOTINFACTION.get());
                    return;
                }
                final List<Player> playerList = FactionIntegration.getInstance().getFactionOnlinePlayers(faction);
                final String rank = FactionIntegration.getInstance().getPlayerRank(player.getUniqueId());
                final String title = FactionIntegration.getInstance().getPlayerTitle(player.getUniqueId());
                playerList.forEach(players -> players.sendMessage(Messages.INFO_FACTION_CHAT_FACTION.get(faction, rank, title, player.getName(), message)));

                AvatarReturnsAPI.get().getPlugin().getServer().getOnlinePlayers().stream().filter(players ->
                        Utils.havePermission(players,Permissions.FACTION_SPY.get(),true) && !playerList.contains(players)
                ).forEach(spy -> spy.sendMessage(Messages.INFO_FACTION_SPY_FACTION.get(faction, rank, title, player.getName(), message)));
            });
        });
    }

    @RegisterCommand(command = "/ff", parametersMin = 1, executorType = RegisterCommand.ExecutorType.PLAYERS)
    public void run(final CommandSender sender, final String[] args) {
        final StringBuilder message = new StringBuilder();
        for (int i = 0; i < args.length; i++) {
            message.append(args[i]);
            if (i < args.length - 1)
                message.append(" ");
        }
        sendMessage((Player) sender, message.toString());
    }
}
