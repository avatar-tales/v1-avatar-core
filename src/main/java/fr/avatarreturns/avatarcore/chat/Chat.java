package fr.avatarreturns.avatarcore.chat;

import fr.avatarreturns.api.AvatarReturnsAPI;
import fr.avatarreturns.api.schedulers.IUpdated;
import fr.avatarreturns.api.storage.databases.IDatabase;
import fr.avatarreturns.api.storage.files.IFileManager;
import fr.avatarreturns.api.users.IUser;
import fr.avatarreturns.avatarcore.AvatarCore;
import fr.avatarreturns.avatarcore.chat.factions.FactionChat;
import fr.avatarreturns.avatarcore.listeners.PluginMessage;
import fr.avatarreturns.avatarcore.storage.databases.DatabaseCreator;
import fr.avatarreturns.avatarcore.storage.files.GeneralConfig;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.libs.org.apache.commons.codec.binary.Hex;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Logger;

public class Chat implements IUpdated {

    private static boolean isCustomChatEnable = true;
    private static boolean factionEnabled = true;
    private static boolean isFactionDisplayEnabled = true;
    private static boolean localChatEnable = true;
    private static boolean colorBendingEnabled = true;
    private static boolean displayGroupEnabled = true;
    private static boolean multiServerEnabled = true;
    private static double maxRange = 30.0;
    private static boolean initi = false;
    private static boolean isDbConnected = false;
    private static String host = "";
    private static int port = 3306;
    private static String pass = "";
    private static String db = "";
    private static String user = "";
    private static int dbserverUuid = -1;
    private static long lastMsgDate = System.currentTimeMillis();
    private static IDatabase database;
    private static Chat instance;
    public static ArrayList<Player> lstSpyChatUser = new ArrayList<>();
    private static Map<String, String> chatPrefix = new HashMap<>();

    private int step;

    private Chat(){}

    public static Chat getInstance(){

        if(instance == null) instance = new Chat();
        return instance;
    }

    public  void start() {

        this.step = 0;
        init();

        AvatarReturnsAPI.get().registerUpdated(this);
    }

    public void stop() {

        this.step = 0;
        AvatarReturnsAPI.get().unregisterUpdated(this);
    }

    public static void init() {

        if (initi) return;

        PluginMessage.get().getAllRanks();

        Optional<IFileManager.IConfig> config = GeneralConfig.getConfig();
        config.ifPresent(conf -> {

                isCustomChatEnable = conf.getConfig().getBoolean("Chat.isCustomChatEnable");
                factionEnabled = conf.getConfig().getBoolean("Chat.factionEnabled");
                isFactionDisplayEnabled = conf.getConfig().getBoolean("Chat.isFactionDisplayEnabled");
                localChatEnable = conf.getConfig().getBoolean("Chat.localChatEnable");
                colorBendingEnabled = conf.getConfig().getBoolean("Chat.colorBendingEnabled");
                displayGroupEnabled = conf.getConfig().getBoolean("Chat.displayGroupEnabled");
                maxRange = conf.getConfig().getDouble("Chat.maxRange");
                multiServerEnabled = conf.getConfig().getBoolean("Chat.MultiServer.enabled");

                AvatarReturnsAPI.get().debug("[AvatarCore] Chat : Default config Loaded");

                if (multiServerEnabled && !isDbConnected) {

                    host = conf.getConfig().getString("Chat.MultiServer.Myslql.host");
                    port = conf.getConfig().getInt("Chat.MultiServer.Myslql.port");
                    pass = conf.getConfig().getString("Chat.MultiServer.Myslql.pass");
                    db = conf.getConfig().getString("Chat.MultiServer.Myslql.db");
                    user = conf.getConfig().getString("Chat.MultiServer.Myslql.user");
                    dbserverUuid = conf.getConfig().getInt("Chat.MultiServer.Save.server-uuid");

                    AvatarReturnsAPI.get().debug("[AvatarCore] Chat : Multi Server config Loaded");

                    database = DatabaseCreator.getInstance().connectMySQLDatabase(AvatarCore.get().getPlugin(), host, user, pass, db, port);

                    if (database.getConnection() == null) {
                        multiServerEnabled = false;
                        AvatarReturnsAPI.get().debug("[AvatarCore] Chat : Disabling multi server chat due to database error ");
                        return;
                    }

                    //Création des tables
                    if (!database.tableExists("avatarCore_ServerList")) {
                        AvatarReturnsAPI.get().debug("[AvatarCore] Creating avatarCore_ServerList table");
                        final String query = "CREATE TABLE `avatarCore_ServerList` (" + "`serverUuid` BIGINT NOT NULL," + "`date` BIGINT NOT NULL ," + "`name` TEXT, PRIMARY KEY (serverUuid));";
                        database.modifyQuery(query, false);
                        AvatarReturnsAPI.get().debug("Ajout de la table avatarCore_ServerList");
                    }


                    if (!database.tableExists("avatarCore_MessageList")) {
                        AvatarReturnsAPI.get().debug("[AvatarCore] Creating avatarCore_MessageList table");
                        final String query = "CREATE TABLE `avatarCore_MessageList` (" + "`id` INT NOT NULL AUTO_INCREMENT, " +
                                "`serverUuid` BIGINT NOT NULL," +
                                "`date` BIGINT NOT NULL ," +
                                "`message` TEXT, " +
                                "CONSTRAINT `aC_MsgLst_Prim` PRIMARY KEY (id),  " +
                                "CONSTRAINT `aC_MsgLst_Frgn` FOREIGN KEY (serverUuid) REFERENCES avatarCore_ServerList (serverUuid) ON DELETE CASCADE );";
                        database.modifyQuery(query, false);
                        AvatarReturnsAPI.get().debug("Ajout de la table avatarCore_MessageList");
                    }

                    if (!database.readQuery("SELECT COUNT(*) FROM avatarCore_ServerList WHERE serverUuid = '" + dbserverUuid + "'").isPresent()) {
                        multiServerEnabled = false;
                        AvatarReturnsAPI.get().debug("[AvatarCore] Chat : Disabling multi server chat due to database error ");
                        return;
                    }

                    int nb = 0;
                    try (ResultSet rs = database.readQuery("SELECT * FROM avatarCore_ServerList WHERE serverUuid = '" + dbserverUuid + "'").get()) {

                        while (rs.next()) {
                            nb++;
                        }

                    } catch (final SQLException e) {
                        e.printStackTrace();
                    }

                    if (nb == 0) { //Il faut  ajouter le serveur a la table

                        try (ResultSet rs = database.readQuery("SELECT * FROM avatarCore_ServerList").get()) {

                            while (rs.next()) {
                                //dbserverUuid = rs.getInt(1);
                                nb++;
                            }

                        } catch (final SQLException e) {
                            e.printStackTrace();
                        }
                        dbserverUuid =  nb++;
                        database.modifyQuery("INSERT INTO avatarCore_ServerList (serverUuid,date,name) VALUES ('" + dbserverUuid + "','" + System.currentTimeMillis() + "','"+Bukkit.getServer().getName()+"')", false);
                        conf.getConfig().set("chat.MultiServer.Save.server-uuid", dbserverUuid);
                        System.out.println("[AvatarCore] Server register as"+dbserverUuid);
                    }
                }

                isDbConnected = true;
        });


        initi = true;
    }


    public static void handle(final AsyncPlayerChatEvent event) {


        init();
        if (!isCustomChatEnable) {

            AvatarReturnsAPI.get().debug("Le chat custom est désactivé");
            return;
        }

        Player sender = event.getPlayer();
        Optional<IUser> senderOptionalUser = AvatarReturnsAPI.get().getOrLoadUser(sender.getUniqueId());
        if (!senderOptionalUser.isPresent()) {
            return;
        }

        event.setCancelled(true);
        String playerMsg = event.getMessage();

        if (factionEnabled) {
            AvatarReturnsAPI.get().debug("Checking players' chat mode... ->");
            if (!FactionChat.isPublicChat(event.getPlayer())) {
                FactionChat.getChatMode(event.getPlayer()).getClazz().sendMessage(event.getPlayer(), playerMsg);
                return;
            }
            AvatarReturnsAPI.get().debug("public");
        }

        final IUser senderUser = senderOptionalUser.get();

        final ArrayList<Player> players = new ArrayList<>(Bukkit.getOnlinePlayers());

        if (localChatEnable) {

            if (playerMsg.charAt(0) == '!') {
                if (playerMsg.substring(1).equalsIgnoreCase("") || playerMsg.substring(1).equalsIgnoreCase(" ")) return;
                playerMsg = playerMsg.substring(1); //Supression du "!"
                while (playerMsg.charAt(0) == ' ') {
                    playerMsg = playerMsg.substring(1);
                }
            } else {

                //Mise a jour des destinataires
                players.clear();

                String finalPlayerMsg = playerMsg;
                Bukkit.getScheduler().runTask(AvatarCore.get().getPlugin(), () -> {
                    for (Entity entity : sender.getNearbyEntities(maxRange, maxRange, maxRange)) {
                        if (entity instanceof Player) {
                            players.add((Player) entity);
                        }
                    }

                    if (!players.contains(sender))
                        players.add(sender);

                    sendMessage(event, sender, finalPlayerMsg, "§a[L]§f ", senderUser, players);
                });
                return;
            }

        }

        sendMessage(event, sender, playerMsg, "", senderUser, players);
    }

    private static void sendMessage(AsyncPlayerChatEvent event, Player sender, String playerMsg, String local, IUser senderUser, ArrayList<Player> players) {
        new Thread(() -> {
            String factionName = "";
            if (factionEnabled && isFactionDisplayEnabled) {

                if (senderUser.getFactionName().isPresent()) {
                    factionName = "§7[" + senderUser.getFactionName().get() + "] ";
                    if (senderUser.getFactionName().get().equals("")) factionName = "§7[Wild] ";
                }

                if (displayGroupEnabled) {

                    if (senderUser.getGroupName().isPresent()) {
                        if (senderUser.getGroupName().get().toLowerCase().equals("tortuelion")) factionName = "";
                    } else if (sender.isOp()) {
                        factionName = "";
                    }
                }
            }

            String playerName = sender.getDisplayName();
            if (colorBendingEnabled) {

                if (senderUser.getBendingName().size() >= 1) {

                    switch (senderUser.getBendingName().get(0).toLowerCase()) {

                        case "fire":
                            playerName = "§c" + playerName;
                            break;

                        case "earth":
                            playerName = "§a" + playerName;
                            break;
                        case "water":
                            playerName = "§9" + playerName;
                            break;

                        case "air":
                            playerName = "§b" + playerName;
                            break;

                        case "chi":
                            playerName = "§e" + playerName;
                            break;
                    }
                } else if (senderUser.getBendingName().size() == 0)
                    playerName = "§7" + playerName;

                if (senderUser.getGroupName().isPresent()) {
                    if (senderUser.getGroupName().get().equals("tortuelion")) {
                        playerName = "§6" + playerName;
                    } else if (senderUser.getBendingName().size() >= 4) {
                        playerName = "§5[Avatar] " + playerName;
                    }
                } else if (!sender.isOp() && senderUser.getBendingName().size() >= 4) {
                    playerName = "§5[Avatar] " + playerName;
                }
            }

            String displayGroup = "";
            if (displayGroupEnabled) {

                if (senderUser.getGroupName().isPresent()) {

                    final String groupName = senderUser.getGroupName().get();
                    try {
                        displayGroup = ChatColor.translateAlternateColorCodes('&', PluginMessage.get().getRankByPlayer(sender.getUniqueId()).get(5 * 1000, TimeUnit.MILLISECONDS));
                    } catch (InterruptedException | ExecutionException | TimeoutException ignored) {
                        if (chatPrefix.containsKey(groupName.toLowerCase()))
                            displayGroup = ChatColor.translateAlternateColorCodes('&', chatPrefix.get(groupName.toLowerCase()));
                        else {
                            try {
                                displayGroup = ChatColor.translateAlternateColorCodes('&', PluginMessage.get().getRankByName(groupName).get(5 * 1000, TimeUnit.MILLISECONDS));
                            } catch (InterruptedException | ExecutionException | TimeoutException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                /*switch (groupName.toLowerCase()) {

                    case "esprit":
                        displayGroup = "§6[TortueLion] ";
                        break;

                    case "dev":
                        displayGroup = "§9[Dev] ";
                        break;

                    case "builder":
                        displayGroup = "§3[Builder] ";
                        break;
                    case "moderation":
                        displayGroup = "§2[Esprit] ";
                        break;
                    case "animateur":
                        displayGroup = "§5[Animateur] ";
                        break;
                }*/

                }
            }

            String title = factionName + displayGroup;

            if (!sender.getDisplayName().equals(sender.getName())) {
                title = "";
            }

            for (Player player : players) {
                player.sendMessage(local + title + playerName + "§f : " + playerMsg);
            }

            Logger.getLogger("Chat").info(local + title + playerName + "§f : " + playerMsg);

            if (localChatEnable && event.getMessage().charAt(0) != '!') {
                sendLocalSpy(players, title, playerName, playerMsg);
            }

            if (multiServerEnabled && isDbConnected) {

                if (localChatEnable && event.getMessage().charAt(0) == '!') {
                    database.modifyQuery("INSERT INTO  `avatarCore_MessageList` (`serverUuid`,`date`,`message`) VALUES ('" + dbserverUuid + "', '" + System.currentTimeMillis() + "' , '" + toHex("    " + title + playerName + "§f : " + playerMsg) + "');", true);
                } else if (!localChatEnable) {
                    database.modifyQuery("INSERT INTO  `avatarCore_MessageList` (`serverUuid`,`date`,`message`) VALUES ('" + dbserverUuid + "', '" + System.currentTimeMillis() + "' , '" + toHex("    " + title + playerName + "§f : " + playerMsg) + "');", true);
                }
            }
        }).start();
    }

    private static void sendLocalSpy(ArrayList<Player> lstLocalPlayer,String title, String playerName, String message){

        if(title.length()>2)title = title.substring(2);
        if(playerName.length()>2) playerName = playerName.substring(2);

        for(Player player : lstSpyChatUser){

            if(lstLocalPlayer.contains(player))continue;
            player.sendMessage("§cSpyChat §7§o"+title+"§7§o"+playerName+"§7§o : "+message);
        }
    }

    public static void sendMultiServerMessage(String msg){
        init();
        database.modifyQuery("INSERT INTO  `avatarCore_MessageList` (`serverUuid`,`date`,`message`) VALUES ('" + dbserverUuid + "', '" + System.currentTimeMillis() + "' , '" + toHex("    "+msg)+"');", true);
    }

    private static String toHex(String arg) {
        return String.format("%040x", new BigInteger(1, arg.getBytes(StandardCharsets.UTF_8)));
    }

    private static String hexaToString(String hexString){
        try {
            byte[] bytes = Hex.decodeHex(hexString.toCharArray());
            return new String(bytes, "UTF-8");
        }
        catch (Exception e){
            System.out.println("[AvatarCore]Erreur Hexa to String :"+hexString);
        }
        return "";
    }

    @Override
    public void run() { //Pour information la méthode run ne doit jamais être appelée directement, donc elle devrait être private

        if(!multiServerEnabled)return;
        if ((step++) % 50 != 0) {
            step = 0;
            return;
        }


        if(!isDbConnected)return; //Ne lis pas de message tant que la db n'est pas connectée

        //AvatarReturnsAPI.get().debug("[AvatarCore] Lecture");

        try (ResultSet rs = database.readQuery("SELECT * FROM avatarCore_MessageList WHERE serverUuid != '" + dbserverUuid + "' AND date > '" +lastMsgDate + "'").get()) {
        //try( ResultSet rs = database.readQuery("SELECT * FROM avatarCore_MessageList where serverUuid != '" + dbserverUuid + "' + AND date > '" +lastMsgDate + "'").get()) {

            while (rs.next()) {
                lastMsgDate = rs.getLong(3);
                for (Player player : Bukkit.getOnlinePlayers()) {
                    String msg =hexaToString(rs.getString(4));
                    if(!msg.equals("")) player.sendMessage(msg.substring(4)); //"4 first charcters are blanc to remove [NULL] char bug"
                    else if(player.isOp()){
                        player.sendMessage("[AvatarCore]Fonction hexaToString : Erreur reception de message non convertible");
                    }
                }
            }

        } catch (final SQLException e) {

        }
    }

    public static boolean isIsCustomChatEnable() {
        return isCustomChatEnable;
    }

    public static boolean isFactionEnabled() {
        return factionEnabled;
    }

    public static boolean isIsFactionDisplayEnabled() {
        return isFactionDisplayEnabled;
    }

    public static boolean isLocalChatEnable() {
        return localChatEnable;
    }

    public static boolean isColorBendingEnabled() {
        return colorBendingEnabled;
    }

    public static boolean isDisplayGroupEnabled() {
        return displayGroupEnabled;
    }

    public static boolean isMultiServerEnabled() {
        return multiServerEnabled;
    }

    public static double getMaxRange() {
        return maxRange;
    }

    public static String getHost() {
        return host;
    }

    public static int getPort() {
        return port;
    }

    public static String getPass() {
        return pass;
    }

    public static String getDb() {
        return db;
    }

    public static String getUser() {
        return user;
    }

    public static int getDbserverUuid() {
        return dbserverUuid;
    }

    public static Map<String, String> getChatPrefix() {
        return chatPrefix;
    }
}
