package fr.avatarreturns.avatarcore;

import fr.avatarreturns.api.AvatarPlugin;
import fr.avatarreturns.api.AvatarReturnsAPI;
import fr.avatarreturns.avatarcore.animators.Animator;
import fr.avatarreturns.avatarcore.chat.Chat;
import fr.avatarreturns.avatarcore.chat.factions.*;
import fr.avatarreturns.avatarcore.commands.*;
import fr.avatarreturns.avatarcore.integrations.*;
import fr.avatarreturns.avatarcore.inventory.Scheduler;
import fr.avatarreturns.avatarcore.listeners.Listening;
import fr.avatarreturns.avatarcore.listeners.PluginMessage;
import fr.avatarreturns.avatarcore.messages.MessagesConfig;
import fr.avatarreturns.avatarcore.permissions.PermissionsConfig;
import fr.avatarreturns.avatarcore.storage.files.GeneralConfig;
import fr.avatarreturns.avatarcore.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.InvalidPluginException;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.SimplePluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.FileFilter;

public class AvatarCorePlugin extends AvatarPlugin {

    public static JavaPlugin get() {
        return AvatarCorePlugin.getPlugin(AvatarCorePlugin.class);
    }

    @Override
    public void onLoad() {
        if (this.getServer().getPluginManager().isPluginEnabled("WorldGuard")) {
            if (this.getServer().getPluginManager().isPluginEnabled("Factions"))
                WorldGuardIntegration.getInstance().createFlag("change-power");
        }
    }

    @Override
    public void onEnable() {

        final AvatarCore core = new AvatarCore(this);
        new Thread(() -> {
            try {
                core.waitUntilStarted();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            MessagesConfig.init();
            PermissionsConfig.init();
            GeneralConfig.init();
            ((AvatarCore) AvatarCore.get()).setDebugMode(GeneralConfig.isDebugMode());
            super.onEnable();
            this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
            AvatarCorePlugin.get().getServer().getMessenger().registerIncomingPluginChannel(AvatarCorePlugin.get(), "BungeeCord", PluginMessage.get());
            AvatarReturnsAPI.get().registerCommandClass(new Broadcast());
            AvatarReturnsAPI.get().registerCommandClass(new Dice());
            AvatarReturnsAPI.get().registerCommandClass(new Lag());
            AvatarReturnsAPI.get().registerCommandClass(new Spawn());
            AvatarReturnsAPI.get().registerCommandClass(new LocalSpy());
            if (AvatarReturnsAPI.get().isIntegrate("Citizens"))
                CitizensIntegration.getInstance();
            if (AvatarReturnsAPI.get().isIntegrate("Factions")) {
                FactionIntegration.getInstance();
                AvatarReturnsAPI.get().registerCommandClass(FactionChatCommand.getInstance(), FactionAlly.getInstance(), FactionFaction.getInstance(), FactionTruce.getInstance());
            }
            if (AvatarReturnsAPI.get().isIntegrate("ProjectKorra"))
                KorraIntegration.getInstance();
            if (AvatarReturnsAPI.get().isIntegrate("LuckPerms"))
                LuckPermsIntegration.getInstance();
            if (AvatarReturnsAPI.get().isIntegrate("Votifier"))
                VotifierIntegration.getInstance();
            if (AvatarReturnsAPI.get().isIntegrate("WorldEdit"))
                WorldEditIntegration.getInstance();
            if (AvatarReturnsAPI.get().isIntegrate("WorldGuard"))
                WorldGuardIntegration.getInstance();
            AvatarReturnsAPI.get().getPlugin().getServer().getPluginManager().registerEvents(new Listening(), this);
            Chat.getInstance().start();
            Animator.create();
            final File file = new File(this.getDataFolder().getAbsolutePath(), File.separator + "modules");
            if (!file.exists())
                file.mkdirs();
            final FileFilter filter = files -> files.getName().endsWith(".jar");
            for (final File files : file.listFiles(filter)) {
                try {
                    if (!files.isFile())
                        continue;
                    final Plugin plugin = ((SimplePluginManager) Bukkit.getPluginManager()).loadPlugin(files);
                    if (plugin == null)
                        continue;
                    Bukkit.getPluginManager().enablePlugin(plugin);
                } catch (InvalidPluginException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        //Chat.lstSpyChatUser = new ArrayList<>();
        for(Player player : this.getServer().getOnlinePlayers()){
            if(Utils.havePermission(player,"avatar.spychat",true)){
                Chat.lstSpyChatUser.add(player);
            }
        }



    }

    @Override
    public void onDisable(){
        Bukkit.getOnlinePlayers().forEach(Player::closeInventory);
        Scheduler.getInstance().stop(this);
        Chat.getInstance().stop();
    }

}
