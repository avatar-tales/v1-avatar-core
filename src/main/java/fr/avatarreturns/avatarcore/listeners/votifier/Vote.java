package fr.avatarreturns.avatarcore.listeners.votifier;

import com.vexsoftware.votifier.model.VotifierEvent;
import com.vexsoftware.votifier.support.forwarding.ForwardedVoteListener;
import fr.avatarreturns.api.AvatarReturnsAPI;
import fr.avatarreturns.api.storage.files.IFileManager;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.minecraft.server.v1_16_R3.PacketPlayOutNamedSoundEffect;
import net.minecraft.server.v1_16_R3.SoundCategory;
import net.minecraft.server.v1_16_R3.SoundEffects;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.craftbukkit.v1_16_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.time.Instant;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;

public class Vote implements Listener, ForwardedVoteListener {

    private IFileManager.IConfig config;

    public Vote() {
        final Optional<IFileManager.IConfig> config = AvatarReturnsAPI.get().getFileManager().createFile(AvatarReturnsAPI.get().getPlugin().getDataFolder().getAbsolutePath(), "votes.yml");
        config.ifPresent(iConfig -> this.config = iConfig);
        if (this.config == null)
            return;
        if (this.config.firstCreation()) {
            this.config.getConfig().set("limit", 30);
            this.config.getConfig().createSection("players");
            this.config.save();
        }
    }

    @Override
    public void onForward(com.vexsoftware.votifier.model.Vote vote) {
        AvatarReturnsAPI.get().debug("Un vote a eu lieu par " + vote.getUsername());
    }

    @EventHandler
    public void onVotifierEvent(final VotifierEvent e) {
        final Optional<UUID> optionalUUID = AvatarReturnsAPI.get().getUtils().getRealUUID(e.getVote().getUsername());
        optionalUUID.ifPresent(uuid ->
            playerVote(Bukkit.getOfflinePlayer(uuid))
        );
    }

    private void playerVote(final OfflinePlayer player) {
        final int random = new Random().nextInt(51) + 50;
        int dailyStreak = 0;
        final long now = Instant.now().getEpochSecond();
        final String uuid = player.getUniqueId().toString().replace("-", "");
        final double limit = this.config.getConfig().getInt("limit");
        if (this.config.getConfig().get("players." + uuid) != null) {
            final long timestamp = this.config.getConfig().getLong("players." + uuid + ".timestamp");
            if (now - timestamp <= 48 * 60 * 60) {
                dailyStreak = this.config.getConfig().getInt("players." + uuid + ".dailyStreak");
            }
        }
        final int somme = (int) Math.ceil(random * (1 + (dailyStreak > limit ? limit : dailyStreak) / limit));
        AvatarReturnsAPI.get().getUserWithoutLoadIt(player.getUniqueId()).addMoney(somme);
        this.config.getConfig().set("players." + uuid + ".timestamp", now);
        this.config.getConfig().set("players." + uuid + ".dailyStreak", ++dailyStreak);

        if (AvatarReturnsAPI.get().isIntegrate("Vault"))
            AvatarReturnsAPI.get().getPlugin().getServer().broadcastMessage("§7[§cAlerte§7] §b§l" + player.getName() + " §7remporte " + somme + "Y (Série : " + dailyStreak + (dailyStreak >= limit ? " MAX" : "") + " vote" + (dailyStreak > 1 ? "s" : "") + ") en votant pour le serveur");
        else
            AvatarReturnsAPI.get().getPlugin().getServer().broadcastMessage("§7[§cAlerte§7] §b§l" + player.getName() + " §7vient de soutenir le serveur en votant." + "(Série : " + dailyStreak + (dailyStreak >= limit ? " MAX" : "") + " vote" + (dailyStreak > 1 ? "s" : "") + ")");

        TextComponent message = new TextComponent("§bCliquez ici pour aller voter");
        message.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "http://vote.avatar-returns.fr"));
        message.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Clique ici pour voter !").create()));
        AvatarReturnsAPI.get().getPlugin().getServer().spigot().broadcast(message); // envoi a tout le monde lien cliquable

        for (final Player players : AvatarReturnsAPI.get().getPlugin().getServer().getOnlinePlayers()) {
            PacketPlayOutNamedSoundEffect sound = new PacketPlayOutNamedSoundEffect(SoundEffects.ENTITY_FIREWORK_ROCKET_LARGE_BLAST, SoundCategory.MASTER, players.getLocation().getX(), players.getLocation().getY(), players.getLocation().getZ(), 10, 1);
            ((CraftPlayer) players).getHandle().playerConnection.sendPacket(sound);
            sound = new PacketPlayOutNamedSoundEffect(SoundEffects.ENTITY_FIREWORK_ROCKET_TWINKLE, SoundCategory.MASTER, players.getLocation().getX(), players.getLocation().getY(), players.getLocation().getZ(), 20, 1);
            ((CraftPlayer) players).getHandle().playerConnection.sendPacket(sound);
        }
    }


}
