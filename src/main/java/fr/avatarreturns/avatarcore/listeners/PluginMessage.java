package fr.avatarreturns.avatarcore.listeners;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import fr.avatarreturns.avatarcore.AvatarCorePlugin;
import fr.avatarreturns.avatarcore.chat.Chat;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;
import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.util.*;
import java.util.concurrent.CompletableFuture;

public class PluginMessage implements PluginMessageListener {

    private final Map<String, LinkedList<CompletableFuture<String>>> completable;
    private final Map<UUID, LinkedList<CompletableFuture<String>>> completableUUID;

    private static PluginMessage instance;

    public static PluginMessage get() {
        if (instance == null)
            instance = new PluginMessage();
        return instance;
    }

    private PluginMessage() {
        completable = new HashMap<>();
        completableUUID = new HashMap<>();
    }

    @Override
    public void onPluginMessageReceived(@NotNull String channel, @NotNull Player player, @NotNull byte[] message) {
        if (!channel.equals("BungeeCord"))
            return;
        String action = null;
        final ArrayList<String> received = new ArrayList<>();
        final DataInputStream in = new DataInputStream(new ByteArrayInputStream(message));
        try {
            action = in.readUTF();
            while (in.available() > 0) {
                received.add(in.readUTF());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (action == null) return;
        if (action.equalsIgnoreCase("receiveRank")) {
            if (received.get(0).equals("byPlayer")) {
                final UUID uuid = UUID.fromString(received.get(1));
                final String value = received.get(2).replace("none", "");
                synchronized (completableUUID) {
                    if (completableUUID.containsKey(uuid)) {
                        final CompletableFuture<String> future = completableUUID.get(uuid).poll();
                        if (future != null)
                            future.complete(value);
                    }
                }
            } else if (received.get(0).equals("byName")) {
                final String rank = received.get(1).toLowerCase();
                final String value = received.get(2).replace("none", "");
                synchronized (completable) {
                    if (completable.containsKey(rank)) {
                        final CompletableFuture<String> future = completable.get(rank).poll();
                        if (future != null)
                            future.complete(value);
                    }
                }
                if (Chat.getChatPrefix().containsKey(rank))
                    Chat.getChatPrefix().replace(rank, value);
                else
                    Chat.getChatPrefix().put(rank, value);
            }
        } else if (action.equalsIgnoreCase("receiveRanks")) {
            for (final String result : received) {
                final String rank = result.split(":")[0].toLowerCase();
                String value = "";
                if (result.split(":").length >= 2)
                    value = result.split(":")[1].replace("none", "");
                if (Chat.getChatPrefix().containsKey(rank))
                    Chat.getChatPrefix().replace(rank, value);
                else
                    Chat.getChatPrefix().put(rank, value);
            }
        }
    }

    public CompletableFuture<String> getRankByName(final String askRank) {
        final String rank = askRank.toLowerCase();
        final CompletableFuture<String> future = new CompletableFuture<>();
        synchronized (completable) {
            if (!completable.containsKey(rank)) {
                completable.put(rank, new LinkedList<>());
            }
            completable.get(rank).add(future);
        }
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("getRank");
        out.writeUTF(askRank);
        AvatarCorePlugin.get().getServer().sendPluginMessage(AvatarCorePlugin.get(), "BungeeCord", out.toByteArray());
        return future;
    }

    public CompletableFuture<String> getRankByPlayer(final UUID player) {
        final CompletableFuture<String> future = new CompletableFuture<>();
        synchronized (completableUUID) {
            if (!completableUUID.containsKey(player)) {
                completableUUID.put(player, new LinkedList<>());
            }
            completableUUID.get(player).add(future);
        }
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("getRank");
        out.writeUTF(player.toString());
        AvatarCorePlugin.get().getServer().sendPluginMessage(AvatarCorePlugin.get(), "BungeeCord", out.toByteArray());
        return future;
    }

    public void getAllRanks() {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("getAllRanks");
        AvatarCorePlugin.get().getServer().sendPluginMessage(AvatarCorePlugin.get(), "BungeeCord", out.toByteArray());
    }

}
