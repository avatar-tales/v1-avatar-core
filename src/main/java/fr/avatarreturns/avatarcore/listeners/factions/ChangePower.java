package fr.avatarreturns.avatarcore.listeners.factions;

import com.massivecraft.factions.event.EventFactionsPowerChange;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.math.BlockVector2;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import fr.avatarreturns.avatarcore.integrations.WorldGuardIntegration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.Objects;

public class ChangePower implements Listener {

    @EventHandler
    public void onFaction(EventFactionsPowerChange e) {
        try {
            final Player player = e.getMPlayer().getPlayer();
            RegionManager regionContainer = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(player.getWorld()));
            if (regionContainer == null) return;
            for (final ProtectedRegion region : regionContainer.getRegions().values()) {
                if (Objects.equals(region.getFlag(WorldGuardIntegration.getInstance().getFlag("change-power")), StateFlag.State.DENY)) {
                    if (region.contains(BlockVector2.at(player.getLocation().getX(), player.getLocation().getZ())) || region.getId().equalsIgnoreCase("__global__")) {
                        e.setCancelled(true);
                    }
                }
            }
        }
        catch (Exception ignored) {
        }
    }


}
