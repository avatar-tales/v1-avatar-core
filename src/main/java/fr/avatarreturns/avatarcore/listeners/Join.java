package fr.avatarreturns.avatarcore.listeners;

import fr.avatarreturns.api.AvatarReturnsAPI;
import fr.avatarreturns.api.storage.files.IFileManager;
import fr.avatarreturns.avatarcore.chat.Chat;
import fr.avatarreturns.avatarcore.events.UserJoinEvent;
import fr.avatarreturns.avatarcore.storage.files.GeneralConfig;
import fr.avatarreturns.avatarcore.utils.Utils;
import io.netty.resolver.AbstractAddressResolver;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.Statistic;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.Optional;

class Join {

    static void handle(final PlayerJoinEvent e) {

        AvatarReturnsAPI.get().loadUser(e.getPlayer().getUniqueId()).ifPresent(iUser ->
            AvatarReturnsAPI.get().getPlugin().getServer().getPluginManager().callEvent(new UserJoinEvent(iUser, e))
        );

        if(Utils.havePermission(e.getPlayer(),"avatar.spychat",true)){
            Chat.lstSpyChatUser.add(e.getPlayer());
        }

        Optional<IFileManager.IConfig> config = GeneralConfig.getConfig();
        config.ifPresent(conf -> {

            //First Connection Event
            if(conf.getConfig().getBoolean("General.FirstJoin.Enabled")) {

                int playerStat = e.getPlayer().getStatistic(Statistic.PLAY_ONE_MINUTE);

                if (playerStat == 0) {
                    Player player = e.getPlayer();

                    player.sendTitle("§6§lBienvenue", "§6Sur AvatarReturns", 5, 50, 5);


                    Bukkit.broadcastMessage("§6§lNous avons un nouveau joueur !");
                    Bukkit.broadcastMessage("§6§lBienvenue " + player.getName() + " sur Avatar Returns !");

                    for (Player p : Bukkit.getOnlinePlayers()) {
                        p.playSound(p.getLocation(), Sound.ENTITY_FIREWORK_ROCKET_TWINKLE, SoundCategory.VOICE, 1, (float) 1.7);
                    }

                    if(conf.getConfig().getBoolean("General.FirstJoin.MultiServer") && conf.getConfig().getBoolean("Chat.MultiServer.enabled")){
                        Chat.sendMultiServerMessage("§6§lNous avons un nouveau joueur !");
                        Chat.sendMultiServerMessage("§6§lBienvenue " + player.getName() + " sur Avatar Returns !");
                    }

                    if (conf.getConfig().getBoolean("Chat.localChatEnable")){
                        player.sendMessage("");
                        player.sendMessage("§c§lAttention");
                        player.sendMessage("§cCe serveur utilise un système de chat local et global");
                        player.sendMessage("");
                        player.sendMessage("§a[L]§7Exemple : Voici un exemple de message local, seuls les joueurs proches le voient");
                        player.sendMessage("§cPour parler en global il faut ajouter un §6§l!§c avant le message");
                    }
                }
            }
        });
    }
}
