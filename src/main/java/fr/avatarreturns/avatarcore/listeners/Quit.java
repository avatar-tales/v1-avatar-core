package fr.avatarreturns.avatarcore.listeners;

import fr.avatarreturns.api.AvatarReturnsAPI;
import fr.avatarreturns.avatarcore.AvatarCore;
import fr.avatarreturns.avatarcore.chat.Chat;
import fr.avatarreturns.avatarcore.events.UserQuitEvent;
import fr.avatarreturns.avatarcore.utils.Utils;
import org.bukkit.event.player.PlayerQuitEvent;

class Quit {

    static void handle(final PlayerQuitEvent e) {
        AvatarReturnsAPI.get().getUser(e.getPlayer().getUniqueId()).ifPresent(user -> {
            AvatarReturnsAPI.get().getPlugin().getServer().getPluginManager().callEvent(new UserQuitEvent(user, e));
            ((AvatarCore) AvatarCore.get()).unload(user);
        });

        if(Utils.havePermission(e.getPlayer(),"avatar.spychat",true)){
            Chat.lstSpyChatUser.remove(e.getPlayer());
        }
    }
}
