package fr.avatarreturns.avatarcore;

import fr.avatarreturns.api.AvatarReturnsAPI;
import fr.avatarreturns.api.commands.ICommandHandler;
import fr.avatarreturns.api.commands.IDefaultCommand;
import fr.avatarreturns.api.inventory.InventoryAPI;
import fr.avatarreturns.api.schedulers.IUpdated;
import fr.avatarreturns.api.scoreboard.IScoreboardSign;
import fr.avatarreturns.api.storage.databases.IDatabaseCreator;
import fr.avatarreturns.api.storage.files.IFileManager;
import fr.avatarreturns.api.users.IUser;
import fr.avatarreturns.api.utils.IMessages;
import fr.avatarreturns.api.utils.IUtils;
import fr.avatarreturns.avatarcore.commands.CommandHandler;
import fr.avatarreturns.avatarcore.commands.DefaultCommand;
import fr.avatarreturns.avatarcore.inventory.Inventory;
import fr.avatarreturns.avatarcore.storage.databases.DatabaseCreator;
import fr.avatarreturns.avatarcore.integrations.vault.VaultIntegration;
import fr.avatarreturns.avatarcore.schedulers.Updater;
import fr.avatarreturns.avatarcore.scoreboard.ScoreboardSign;
import fr.avatarreturns.avatarcore.users.User;
import fr.avatarreturns.avatarcore.storage.files.FileManager;
import fr.avatarreturns.avatarcore.utils.Messages;
import fr.avatarreturns.avatarcore.utils.Utils;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.*;

public class AvatarCore extends AvatarReturnsAPI {

    private final List<String> pluginsIntegrate;
    private final List<IUser> users;

    private boolean started = false;

    public AvatarCore(final JavaPlugin plugin) {
        super(plugin);
        this.debugMode = true;
        final long instant = System.currentTimeMillis();
        this.debug("Enabling... (0ms)");
        this.debug("Defining variables... (" + estimateTime(instant) + "ms)");
        this.pluginsIntegrate = new ArrayList<>();
        this.users = new ArrayList<>();
        this.debug("Verifying integrations... (" + estimateTime(instant) + "ms)");
        if (this.getPlugin().getServer().getPluginManager().isPluginEnabled("Vault"))
            if (VaultIntegration.getInstance().searchEconomy())
                this.pluginsIntegrate.add("vault");
        this.debug("VAULT integration is : " + (this.pluginsIntegrate.contains("vault") ? "ENABLED" : "DISABLED") + " (" + estimateTime(instant) + "ms)");
        for (final String pluginToCheck : new String[]{"Citizens", "Factions", "LuckPerms", "ProjectKorra", "Votifier", "WorldEdit", "WorldGuard"}) {
            if (this.getPlugin().getServer().getPluginManager().isPluginEnabled(pluginToCheck))
                this.pluginsIntegrate.add(pluginToCheck.toLowerCase());
            this.debug(pluginToCheck.toUpperCase() + " integration is : " + (this.pluginsIntegrate.contains(pluginToCheck.toLowerCase()) ? "ENABLED" : "DISABLED") + " (" + estimateTime(instant) + "ms)");
        }
        this.debug("Registering users... (" + estimateTime(instant) + "ms)");
        this.getPlugin().getServer().getOnlinePlayers().forEach(player -> users.add(new User(player.getUniqueId())));
        this.debug("Enabled (" + estimateTime(instant) + "ms)");

        this.started = true;
    }

    @Override
    public Optional<IUser> getUser(final UUID uuid) {
        this.debug("Searching one user...");
        return this.users.stream().filter(iUser -> iUser.getUniqueId().equals(uuid)).findFirst();
    }

    @Override
    public Optional<IUser> getOrLoadUser(final UUID uuid) {
        this.debug("Getting one user... ->");
        final Optional<IUser> finalUser = getUser(uuid);
        if (finalUser.isPresent()) {
            this.debug("success.");
            return finalUser;
        }
        this.debug("failed. Loading one user.");
        return loadUser(uuid);
    }

    @Override
    public IUser getUserWithoutLoadIt(final UUID uuid) {
        this.debug("Loading one player");
        return new User(uuid);
    }

    @Override
    public Optional<IUser> loadUser(final Player player) {
        this.debug("Registering one player");
        if (this.getUser(player).isPresent()) {
            this.debug("Already loaded");
            return Optional.empty();
        }
        return this.loadUser(player.getUniqueId());
    }

    @Override
    public Optional<IUser> loadUser(final UUID uuid) {
        final IUser user = this.getUserWithoutLoadIt(uuid);
        this.debug("Registering one user");
        this.users.add(user);
        return Optional.of(user);
    }

    public Optional<IUser> load(final IUser iUser) {
        return this.load(iUser);
    }

    @Override
    public IUser unloadUser(final IUser iUser) {
        this.debug("Unregistering one user");
        this.users.remove(iUser);
        return iUser;
    }

    public IUser unload(final IUser iUser) {
        return this.unloadUser(iUser);
    }

    @Override
    public IUtils getUtils() {
        return Utils.getInstance();
    }

    @Override
    public void registerUpdated(final IUpdated... iUpdateds) {
        Updater.getInstance().addUpdated(iUpdateds);
    }

    @Override
    public void unregisterUpdated(final IUpdated... iUpdateds) {
        Updater.getInstance().removeUpdated(iUpdateds);
    }

    @Override
    public boolean isIntegrate(final String plugin) {
        final boolean b = this.pluginsIntegrate.contains(plugin.toLowerCase());
        if (b)
            AvatarReturnsAPI.get().debug(plugin.toUpperCase() + " is integrated !");
        else {
            if (this.getPlugin().getServer().getPluginManager().isPluginEnabled(plugin))
                this.pluginsIntegrate.add(plugin.toLowerCase());
            if (this.pluginsIntegrate.contains(plugin.toLowerCase())) {
                AvatarReturnsAPI.get().debug(plugin.toUpperCase() + " is integrated !");
                return true;
            }
            AvatarReturnsAPI.get().debug(plugin.toUpperCase() + " isn't integrated !");
        }
        return b;
    }

    @Override
    public IFileManager getFileManager() {
        return FileManager.getInstance();
    }

    @Override
    public void registerCommandClass(ICommandHandler... iCommandHandlers) {
        Arrays.stream(iCommandHandlers).forEach(CommandHandler.getInstance(this.getPlugin())::registerCommands);
    }

    @Override
    public void unregisterCommandClass(ICommandHandler... iCommandHandlers) {
        Arrays.stream(iCommandHandlers).forEach(CommandHandler.getInstance(this.getPlugin())::unregisterCommands);
    }

    @Override
    public IDatabaseCreator getDatabaseCreator() {
        return DatabaseCreator.getInstance();
    }

    @Override
    public IMessages getMessages() {
        return Messages.getInstance();
    }

    @Override
    public IScoreboardSign createScoreboardSign(final Player player, final String objectiveName) {
        return new ScoreboardSign(player, objectiveName);
    }

    @Override
    public InventoryAPI getInventory(JavaPlugin javaPlugin) {
        return Inventory.create(javaPlugin);
    }

    @Override
    public IDefaultCommand getDefaultCommand() {
        return DefaultCommand.getInstance();
    }

    private long estimateTime(final long start) {
        return System.currentTimeMillis() - start;
    }

    public void waitUntilStarted() throws InterruptedException {
        while (!this.started) {
            Thread.sleep(50);
        }
    }

    void setDebugMode(final boolean debug) {
        this.debugMode = debug;
    }

}
