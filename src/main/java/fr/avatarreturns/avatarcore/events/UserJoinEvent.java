package fr.avatarreturns.avatarcore.events;

import fr.avatarreturns.api.events.AbstractUserJoinEvent;
import fr.avatarreturns.api.users.IUser;
import org.bukkit.event.player.PlayerJoinEvent;

public class UserJoinEvent extends AbstractUserJoinEvent {

    private final IUser user;
    private final PlayerJoinEvent event;

    public UserJoinEvent(final IUser user, final PlayerJoinEvent event) {
        this.user = user;
        this.event = event;
    }

    @Override
    public IUser getUser() {
        return this.user;
    }

    @Override
    public PlayerJoinEvent getEvent() {
        return this.event;
    }
}
