package fr.avatarreturns.avatarcore.events;

import fr.avatarreturns.api.events.AbstractUserLeaveEvent;
import fr.avatarreturns.api.users.IUser;
import org.bukkit.event.player.PlayerQuitEvent;

public class UserQuitEvent extends AbstractUserLeaveEvent {

    private final IUser user;
    private final PlayerQuitEvent event;

    public UserQuitEvent(final IUser user, final PlayerQuitEvent event) {
        this.user = user;
        this.event = event;
    }

    @Override
    public IUser getUser() {
        return this.user;
    }

    @Override
    public PlayerQuitEvent getEvent() {
        return this.event;
    }

}
