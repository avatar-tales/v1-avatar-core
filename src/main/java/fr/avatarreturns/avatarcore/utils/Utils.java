package fr.avatarreturns.avatarcore.utils;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import fr.avatarreturns.api.AvatarReturnsAPI;
import fr.avatarreturns.api.users.IUser;
import fr.avatarreturns.api.utils.IUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.util.Vector;

import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.*;

public class Utils implements IUtils {

    private static Utils instance;

    public static Utils getInstance() {
        if (instance == null)
            instance = new Utils();
        return instance;
    }

    private Utils() {
    }

    @Override
    public Optional<String> getUUID(final String name) {
        AvatarReturnsAPI.get().debug("Searching for " + name + "'s uuid... ->");
        try {
            final URL urlToGetUUID = new URL("https://api.mojang.com/users/profiles/minecraft/" + name);
            final InputStreamReader readerUUID = new InputStreamReader(urlToGetUUID.openStream());
            AvatarReturnsAPI.get().debug("success.");
            return Optional.ofNullable(new JsonParser().parse(readerUUID).getAsJsonObject().get("id").getAsString());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        AvatarReturnsAPI.get().debug("failed.");
        return Optional.empty();
    }

    @Override
    public Optional<UUID> getRealUUID(final String name) {
        final Optional<String> uuid = this.getUUID(name);
        if (!uuid.isPresent())
            return Optional.empty();
        return this.getFullUUID(uuid.get());
    }

    @Override
    public Optional<UUID> getFullUUID(final String uuid) {
        AvatarReturnsAPI.get().debug("Trying convert uuid " + uuid + "... ->");
        if (uuid == null) {
            AvatarReturnsAPI.get().debug("failed.");
            return Optional.empty();
        }
        try {
            final StringBuilder fullUuid = new StringBuilder();
            for (int index = 0; index < uuid.toCharArray().length; index++) {
                fullUuid.append(uuid.charAt(index));
                if (index == 7 || index == 11 || index == 15 || index == 19)
                    fullUuid.append("-");
            }
            AvatarReturnsAPI.get().debug("success.");
            return Optional.of(UUID.fromString(fullUuid.toString()));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        AvatarReturnsAPI.get().debug("failed.");
        return Optional.empty();
    }

    @Override
    public Optional<String[]> getSkin(final Player player) {
        try {
            AvatarReturnsAPI.get().debug("Getting " + player.getName() + "'s skin... ->");
            final String bukkitVersion = Bukkit.getBukkitVersion().replace("-SNAPSHOT", "");
            final Object craftPlayer = Class.forName("org.bukkit.craftbukkit." + bukkitVersion + ".entity.CraftPlayer")
                    .getMethod("getHandle")
                    .invoke(player);
            final GameProfile gameProfile;
            gameProfile = (GameProfile) craftPlayer.getClass().getDeclaredField("profile").get(craftPlayer);
            final Property property = gameProfile.getProperties().get("textures").iterator().next();
            final String value = property.getValue();
            final String signature = property.getSignature();
            AvatarReturnsAPI.get().debug("success.");
            return Optional.of(new String[]{value, signature});
        } catch (NoSuchFieldException | IllegalAccessException | InvocationTargetException | NoSuchMethodException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        AvatarReturnsAPI.get().debug("failed.");
        return Optional.empty();
    }

    @Override
    public Optional<String[]> getSkin(final String name) {
        AvatarReturnsAPI.get().debug("Getting " + name + "'s skin... ->");
        final Optional<String> uuid = getUUID(name);
        if (uuid.isPresent()) {
            try {
                final URL urlToGetSkin = new URL("https://sessionserver.mojang.com/session/minecraft/profile/" + uuid.get() + "?unsigned=false");
                final InputStreamReader readerSkin = new InputStreamReader(urlToGetSkin.openStream());
                final JsonObject property = new JsonParser().parse(readerSkin).getAsJsonObject().get("properties").getAsJsonArray().get(0).getAsJsonObject();
                final String value = property.get("value").getAsString();
                final String signature = property.get("signature").getAsString();
                AvatarReturnsAPI.get().debug("success.");
                return Optional.of(new String[]{value, signature});
            } catch (Exception ignored) {
            }
        }
        AvatarReturnsAPI.get().debug("failed.");
        return Optional.empty();
    }

    public static boolean havePermission(Player player, String permission){
        return havePermission(player,permission,false);
    }

    public static boolean havePermission(Player player, String permission, boolean spiritOnly){

        final Optional<IUser> user = AvatarReturnsAPI.get().getUser((Player) player);
        if (user.isPresent() && spiritOnly) {
            if (user.get().getGroupName().isPresent()) {
                if (user.get().getGroupName().get().toLowerCase().equals("tortuelion")){
                    return true;
                }
            }
            else if(player.hasPermission(permission)){
                return true;
            }
        }
        else{
            if(player.hasPermission(permission)){
                return true;
            }
        }
        return false;
    }


    @Override
    public ItemStack getItem(final Material material) {
        return this.getItem(material, material.name(), 1, new ArrayList<>());
    }

    @Override
    public ItemStack getItem(final Material material, final String name) {
        return this.getItem(material, name, 1, new ArrayList<>());
    }

    @Override
    public ItemStack getItem(final Material material, final int amount) {
        return this.getItem(material, material.name(), amount, new ArrayList<>());
    }

    @Override
    public ItemStack getItem(final Material material, final List<String> lore) {
        return this.getItem(material, material.name(), 1, lore);
    }

    @Override
    public ItemStack getItem(final Material material, final int amount, final List<String> lore) {
        return this.getItem(material, material.name(), amount, lore);
    }

    @Override
    public ItemStack getItem(final Material material, final String name, final int amount) {
        return this.getItem(material, name, amount, new ArrayList<>());
    }

    @Override
    public ItemStack getItem(final Material material, final String name, final List<String> lore) {
        return this.getItem(material, name, 1, lore);
    }

    @Override
    public ItemStack getItem(final Material material, final String name, final int amount, final List<String> lore) {
        if (material.equals(Material.AIR) || material.equals(Material.CAVE_AIR) || material.equals(Material.VOID_AIR))
            return new ItemStack(material);
        final ItemStack itemStack = new ItemStack(material, amount);
        final ItemMeta itemMeta = itemStack.getItemMeta();
        assert itemMeta != null;
        itemMeta.setDisplayName(name);
        itemMeta.setLore(lore);
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

    @Override
    public Optional<ItemStack> getSkull(String s) {
        if(!s.startsWith("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUv")) {
            s = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUv" + s;
        }
        final ItemStack itemStack = new ItemStack(Material.PLAYER_HEAD, 1);
        final SkullMeta itemMeta = (SkullMeta)itemStack.getItemMeta();
        final GameProfile gameProfile = new GameProfile(UUID.randomUUID(), null);
        gameProfile.getProperties().put("textures", new Property("textures", s));
        try {
            assert itemMeta != null;
            final Field declaredField = itemMeta.getClass().getDeclaredField("profile");
            declaredField.setAccessible(true);
            declaredField.set(itemMeta, gameProfile);
        }
        catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        itemStack.setItemMeta(itemMeta);
        return Optional.of(itemStack);
    }

    @Override
    public Optional<ItemStack> getSkull(final UUID uuid) {
        final OfflinePlayer player = Bukkit.getOfflinePlayer(uuid);
        if (player.getName() == null)
            return Optional.empty();
        final ItemStack itemStack = this.getItem(Material.PLAYER_HEAD);
        final SkullMeta skull = (SkullMeta) itemStack.getItemMeta();
        assert skull != null;
        skull.setOwningPlayer(player);
        itemStack.setItemMeta(skull);
        return Optional.of(itemStack);
    }

    @Override
    public ItemStack modifyItem(final ItemStack itemStack, final String name) {
        final ItemMeta itemMeta = itemStack.getItemMeta();
        assert itemMeta != null;
        return this.modifyItem(itemStack, name, itemStack.getAmount(), itemMeta.getLore());
    }

    @Override
    public ItemStack modifyItem(final ItemStack itemStack, final int amount) {
        final ItemMeta itemMeta = itemStack.getItemMeta();
        assert itemMeta != null;
        return this.modifyItem(itemStack, itemMeta.getDisplayName(), amount, itemMeta.getLore());
    }

    @Override
    public ItemStack modifyItem(final ItemStack itemStack, final List<String> lore) {
        final ItemMeta itemMeta = itemStack.getItemMeta();
        assert itemMeta != null;
        return this.modifyItem(itemStack, itemMeta.getDisplayName(), itemStack.getAmount(), lore);
    }

    @Override
    public ItemStack modifyItem(final ItemStack itemStack, final String name, int amount) {
        final ItemMeta itemMeta = itemStack.getItemMeta();
        assert itemMeta != null;
        return this.modifyItem(itemStack, name, amount, itemMeta.getLore());
    }

    @Override
    public ItemStack modifyItem(final ItemStack itemStack, final String name, final List<String> lore) {
        final ItemMeta itemMeta = itemStack.getItemMeta();
        assert itemMeta != null;
        return this.modifyItem(itemStack, name, itemStack.getAmount(), lore);
    }

    @Override
    public ItemStack modifyItem(final ItemStack itemStack, final int amount, final List<String> lore) {
        final ItemMeta itemMeta = itemStack.getItemMeta();
        assert itemMeta != null;
        return this.modifyItem(itemStack, itemMeta.getDisplayName(), amount, lore);
    }

    @Override
    public ItemStack modifyItem(final ItemStack itemStack, final String name, final int amount, final List<String> lore) {
        final ItemMeta itemMeta = itemStack.getItemMeta();
        assert itemMeta != null;
        itemMeta.setDisplayName(name);
        itemMeta.setLore(lore);
        itemStack.setAmount(amount);
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

    @Override
    public String convertToString(final double value) {
        StringTokenizer t=new StringTokenizer(String.valueOf(value),".");
        String s1 = t.nextToken();
        String s2 = t.nextToken();
        int n2 = s2.length();
        if(n2!=1) {
            if(s2.charAt(s2.length()-1) == '0')
                n2 = s2.length()-1;
        }
        if(n2 == 1 && s2.charAt(0) == '0') {
            return s1;
        }
        return value + "";

    }

    @Override
    public <T> List<T> copy(final List<T> data) {
        final List<T> copied = new ArrayList<>();
        for (final T t : data) {
            final boolean add = copied.add(t);
        }
        return copied;
    }

    @Override
    public <T> List<T> copy(final T[] data) {
        final List<T> copied = new ArrayList<>();
        for (final T t : data) {
            final boolean add = copied.add(t);
        }
        return copied;
    }

    @Override
    public Vector rotateAroundAxisX(Vector v, double angle) {
        double y, z, cos, sin;
        cos = Math.cos(angle);
        sin = Math.sin(angle);
        y = v.getY() * cos - v.getZ() * sin;
        z = v.getY() * sin + v.getZ() * cos;
        return v.setY(y).setZ(z);
    }

    @Override
    public Vector rotateAroundAxisY(Vector v, double angle) {
        double x, z, cos, sin;
        cos = Math.cos(angle);
        sin = Math.sin(angle);
        x = v.getX() * cos + v.getZ() * sin;
        z = v.getX() * -sin + v.getZ() * cos;
        return v.setX(x).setZ(z);
    }

    @Override
    public Vector rotateAroundAxisZ(Vector v, double angle) {
        double x, y, cos, sin;
        cos = Math.cos(angle);
        sin = Math.sin(angle);
        x = v.getX() * cos - v.getY() * sin;
        y = v.getX() * sin + v.getY() * cos;
        return v.setX(x).setY(y);
    }

    @Override
    public Vector rotateAroundAxisX(Vector v, double cos, double sin) {
        double y = v.getY() * cos - v.getZ() * sin;
        double z = v.getY() * sin + v.getZ() * cos;
        return v.setY(y).setZ(z);
    }

    @Override
    public Vector rotateAroundAxisY(Vector v, double cos, double sin) {
        double x = v.getX() * cos + v.getZ() * sin;
        double z = v.getX() * -sin + v.getZ() * cos;
        return v.setX(x).setZ(z);
    }

    @Override
    public Vector rotateAroundAxisZ(Vector v, double cos, double sin) {
        double x = v.getX() * cos - v.getY() * sin;
        double y = v.getX() * sin + v.getY() * cos;
        return v.setX(x).setY(y);
    }

    @Override
    public Vector perp(Vector onto, Vector u) {
        return u.clone().subtract(proj(onto, u));
    }

    @Override
    public Vector proj(Vector onto, Vector u) {
        return onto.clone().multiply(onto.dot(u) / onto.lengthSquared());
    }

    @Override
    public Vector from(final Location start, final Location end) {
        final double x = end.getX() - start.getX();
        final double y = end.getY() - start.getY();
        final double z = end.getZ() - start.getZ();
        return new Vector(x, y, z);
    }

}
