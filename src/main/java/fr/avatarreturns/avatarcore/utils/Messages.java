package fr.avatarreturns.avatarcore.utils;

import fr.avatarreturns.api.utils.IMessages;
import org.bukkit.ChatColor;

public class Messages implements IMessages {

    private static Messages instance;

    public static Messages getInstance() {
        if (instance == null)
            instance = new Messages();
        return instance;
    }

    private Messages() {
    }

    @Override
    public String getNoPermission() {
        return fr.avatarreturns.avatarcore.messages.Messages.ERROR_PERMISSION.get();
    }

    @Override
    public String getNotEnoughArguments() {
        return fr.avatarreturns.avatarcore.messages.Messages.ERROR_COMMAND_ARGUMENT.get();
    }

    @Override
    public String getNeedPlayer() {
        return ChatColor.translateAlternateColorCodes('&', "&c&lAvatarSecurity &4&l» &cCette commande n'est accessible qu'aux joueurs.");
    }

    @Override
    public String getNeedConsole() {
        return ChatColor.translateAlternateColorCodes('&', "&c&lAvatarSecurity &4&l» &cCette commande n'est accessible qu'à la console");
    }

    @Override
    public String getCommandNotFound() {
        return fr.avatarreturns.avatarcore.messages.Messages.ERROR_COMMAND_ERROR.get();
    }

    @Override
    public String getReflectionError() {
        return fr.avatarreturns.avatarcore.messages.Messages.ERROR_UNKNOWN.get();
    }

    @Override
    public String getCommandDisabled() {
        return fr.avatarreturns.avatarcore.messages.Messages.ERROR_COMMAND_DISABLED.get();
    }
}
