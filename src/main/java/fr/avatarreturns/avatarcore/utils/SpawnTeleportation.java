package fr.avatarreturns.avatarcore.utils;

import fr.avatarreturns.api.AvatarReturnsAPI;
import fr.avatarreturns.api.storage.files.IFileManager;
import fr.avatarreturns.api.users.IUser;
import fr.avatarreturns.avatarcore.AvatarCore;
import fr.avatarreturns.avatarcore.messages.Messages;
import fr.avatarreturns.avatarcore.permissions.Permissions;
import fr.avatarreturns.avatarcore.storage.files.GeneralConfig;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Server;
import org.bukkit.entity.Player;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

public class SpawnTeleportation {

    private final Server avatarCore;
    private final Player player;
    private final Location location;
    private int taskOne;
    private int taskTwo;
    private boolean canMoved;
    private boolean quickPermission;
    private int teleportDuration = 50;

    public SpawnTeleportation(final Player player) {
        this.avatarCore = Bukkit.getServer();
        this.player = player;
        this.location = player.getLocation().getBlock().getLocation().clone();
        this.canMoved = false;

        Optional<IFileManager.IConfig> config = GeneralConfig.getConfig();
        config.ifPresent(conf -> {
            this.canMoved = !conf.getConfig().getBoolean("spawnIsDenyWhenMoved");
            this.teleportDuration = conf.getConfig().getInt("spawnTimeToExecute");
        });

        quickPermission = Utils.havePermission(player, Permissions.SPAWN_QUICK.get(),true);
        this.start();
    }

    private void start() {

        final AtomicInteger time = new AtomicInteger(teleportDuration);
        final Location location = this.player.getLocation().getBlock().getLocation();

        this.taskOne = this.avatarCore.getScheduler().scheduleSyncRepeatingTask(AvatarCore.get().getPlugin(), () -> {

            if (this.taskOne == -1) return;
            if (!this.location.equals(player.getLocation().getBlock().getLocation()) && !this.canMoved){
                if(!quickPermission) {
                    final AtomicInteger timeReset = new AtomicInteger(10);
                    this.taskTwo = this.avatarCore.getScheduler().scheduleSyncRepeatingTask(AvatarCore.get().getPlugin(), () -> {

                        if (this.taskTwo == -1) return;
                        if (timeReset.get() == 0) {
                            this.avatarCore.getScheduler().cancelTask(this.taskTwo);
                            this.taskTwo = -1;
                            return;
                        }
                        ActionBar.send(this.player, Messages.ERROR_SPAWN_CANCEL_ON_MOVED.get());
                        timeReset.getAndDecrement();

                    }, 0L, 2L);

                    this.avatarCore.getScheduler().cancelTask(this.taskOne);
                    this.taskOne = -1;
                    return;
                }
            }

            final int seconds = (int) Math.ceil(time.get() / 10D);
            ActionBar.send(this.player, "§aTéléportation dans " + seconds + " seconde" + (seconds > 1 ? "s" : ""));

            if (time.get() == 0 || quickPermission) {

                final Optional<IUser> user = AvatarReturnsAPI.get().getUser((Player) player);
                if (user.isPresent()) {
                    final boolean vaultIntegration = AvatarReturnsAPI.get().isIntegrate("Vault");
                    if (vaultIntegration)
                        if (user.get().getMoney() < GeneralConfig.getSpawnCost()) {
                            player.sendMessage(Messages.ERROR_MONEY.get());
                            return;
                        }
                    player.teleport(GeneralConfig.getSpawnLocation());
                    if (vaultIntegration) {
                        user.get().removeMoney(GeneralConfig.getSpawnCost());
                        player.sendMessage(Messages.INFO_MONEY.get(AvatarReturnsAPI.get().getUtils().convertToString(user.get().getMoney())));
                    }
                    player.sendMessage(Messages.INFO_SPAWN_TELEPORT.get());

                }
                this.avatarCore.getScheduler().cancelTask(this.taskOne);
                this.taskOne = -1;
                return;
            }

            time.getAndDecrement();
        }, 0L, 2L);
    }
}
