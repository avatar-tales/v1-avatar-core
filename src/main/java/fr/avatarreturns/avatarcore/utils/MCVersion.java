package fr.avatarreturns.avatarcore.utils;

import org.bukkit.Bukkit;

public enum MCVersion {

    v0("null"),
    v1_7("1.7"),
    v1_8("1.8"),
    v1_9("1.9"),
    v1_10("1.10"),
    v1_11("1.11"),
    v1_12("1.12"),
    v1_13("1.13"),
    v1_14("1.14"),
    v1_15("1.15"),
    v1_16("1.16"),
    v1_17("1.17"),
    v1_18("1.18");

    private final String version;

    MCVersion(final String version) {
        this.version = version;
    }

    public static MCVersion get() {
        final String v = Bukkit.getVersion().split("MC: ")[1].replaceAll("\\)", "");
        for (final MCVersion value : values()) {
            if (v.startsWith(value.getVersion())) {
                return value;
            }
        }
        return MCVersion.v0;
    }

    private String getVersion() {
        return this.version;
    }

    public boolean isSuperior(final MCVersion a) {
        return this.ordinal() > a.ordinal();
    }

    public boolean isInferior(final MCVersion a) {
        return this.ordinal() < a.ordinal();
    }
}

