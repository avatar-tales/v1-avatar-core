package fr.avatarreturns.avatarcore.schedulers;

import fr.avatarreturns.api.AvatarReturnsAPI;
import fr.avatarreturns.api.schedulers.IUpdated;
import org.bukkit.Bukkit;

import java.util.*;

public class Updater {

    private static Updater instance;

    public static Updater getInstance() {
        if (instance == null)
            instance = new Updater();
        return instance;
    }

    final List<IUpdated> toUpdate;

    final Timer timer;
    final TimerTask timerTask;

    boolean running;

    private Updater() {
        this.toUpdate = new ArrayList<>();
        this.timer = new Timer();
        this.timerTask = new TimerTask() {
            @Override
            public void run() {
                toUpdate.forEach(update -> Bukkit.getScheduler().scheduleSyncDelayedTask(AvatarReturnsAPI.get().getPlugin(), update::run, 0L));
            }
        };
        this.running = false;
    }

    public void start() {
        if (this.running)
            this.timer.cancel();
        this.timer.schedule(this.timerTask, 0L, 50L);
    }

    public void stop() {
        if (!this.running)
            this.timer.cancel();
    }

    public void addUpdated(IUpdated... iUpdateds) {
        Arrays.stream(iUpdateds).forEach(update -> {
            this.toUpdate.add(update);
            if (this.toUpdate.size() == 1)
                this.start();
        });
    }

    public void removeUpdated(IUpdated... iUpdateds) {
        Arrays.stream(iUpdateds).forEach(this.toUpdate::remove);
        if (this.toUpdate.size() == 0)
            this.stop();
    }
}
