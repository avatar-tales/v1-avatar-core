package fr.avatarreturns.avatarcore.users;

import fr.avatarreturns.api.AvatarReturnsAPI;
import fr.avatarreturns.api.users.IUser;
import fr.avatarreturns.avatarcore.integrations.FactionIntegration;
import fr.avatarreturns.avatarcore.integrations.KorraIntegration;
import fr.avatarreturns.avatarcore.integrations.LuckPermsIntegration;
import fr.avatarreturns.avatarcore.integrations.vault.VaultIntegration;
import org.bukkit.entity.Player;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class User implements IUser {

    private final UUID uuid;

    public User(final UUID uuid) {
        this.uuid = uuid;
    }

    @Override
    public UUID getUniqueId() {
        return uuid;
    }

    @Override
    public void addMoney(final double money) {
        if (!AvatarReturnsAPI.get().isIntegrate("Vault")) {
            return;
        }
        VaultIntegration.getInstance().addMoney(this.uuid, money);
    }

    @Override
    public void removeMoney(final double money) {
        if (!AvatarReturnsAPI.get().isIntegrate("Vault")) {
            return;
        }
        VaultIntegration.getInstance().removeMoney(this.uuid, money);
    }

    @Override
    public double getMoney() {
        if (!AvatarReturnsAPI.get().isIntegrate("Vault")) {
            return 0;
        }
        return VaultIntegration.getInstance().getMoney(this.uuid);
    }

    @Override
    public Optional<String> getFactionName() {
        if (!AvatarReturnsAPI.get().isIntegrate("Factions")) {
            return Optional.empty();
        }
        return Optional.ofNullable(FactionIntegration.getInstance().getFactionName(this.uuid));
    }

    @Override
    public Optional<String> getGroupName() {
        if (!AvatarReturnsAPI.get().isIntegrate("LuckPerms")) {
            return Optional.empty();
        }
        return Optional.ofNullable(LuckPermsIntegration.getInstance().getGroupName(this.uuid));
    }


    @Override
    public List<String> getBendingName() {
        if (!AvatarReturnsAPI.get().isIntegrate("ProjectKorra")) {
            return new ArrayList<>();
        }
        return KorraIntegration.getInstance().getPlayerBend(this.uuid);
    }

    @Override
    public void sendToServer(final String serverName) {
        this.getPlayer().ifPresent(player -> {
            final ByteArrayOutputStream b = new ByteArrayOutputStream();
            final DataOutputStream out = new DataOutputStream(b);
            try {
                out.writeUTF("Connect");
                out.writeUTF(serverName);
            } catch (IOException e) {
                e.printStackTrace();
            }
            player.sendPluginMessage(AvatarReturnsAPI.get().getPlugin(), "BungeeCord", b.toByteArray());
        });
    }

    private Optional<Player> getPlayer() {
        return Optional.ofNullable(AvatarReturnsAPI.get().getPlugin().getServer().getPlayer(this.uuid));
    }
}
