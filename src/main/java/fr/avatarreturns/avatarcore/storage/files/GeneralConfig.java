package fr.avatarreturns.avatarcore.storage.files;

import fr.avatarreturns.api.AvatarReturnsAPI;
import fr.avatarreturns.api.storage.files.IFileManager;
import fr.avatarreturns.avatarcore.AvatarCore;
import fr.avatarreturns.avatarcore.chat.Chat;
import org.bukkit.Location;
import org.bukkit.configuration.file.YamlConfiguration;

import java.util.*;

public class GeneralConfig {

    private static Optional<IFileManager.IConfig> config;

    private static boolean debug = false;
    private static double spawnCost = 200D;
    private static Location spawnLocation;
    private static long spawnTimeToExecute = 50;
    private static boolean spawnIsDenyWhenMoved = true;
    private static List<String> spawnBlackListWorld = Collections.singletonList("world_the_end");

    public static void init() {
        config = AvatarReturnsAPI.get().getFileManager().createFile(AvatarCore.get().getPlugin().getDataFolder().getAbsolutePath(), "config.yml");
        config.ifPresent(conf -> {
            save();
            debug = conf.getConfig().getBoolean("debug");
            spawnCost = conf.getConfig().getDouble("spawnCost");
            spawnLocation = conf.getConfig().getLocation("spawnLocation");
            spawnBlackListWorld = conf.getConfig().getStringList("spawnBlackListedWorld");
            spawnTimeToExecute = conf.getConfig().getLong("spawnTimeToExecute");
            spawnIsDenyWhenMoved = conf.getConfig().getBoolean("spawnBlackListedWorld");
        });
    }


    public static void save() {
        initConfig();
        config.ifPresent(IFileManager.IConfig::save);
    }

    public static boolean isDebugMode() {
        return debug;
    }

    public static double getSpawnCost() {
        return spawnCost;
    }

    public static Location getSpawnLocation() {
        return spawnLocation;
    }

    public static void setSpawnLocation(final Location spawnLocation) {
        GeneralConfig.spawnLocation = spawnLocation;
        GeneralConfig.save();
    }

    public static List<String> getSpawnBlackListWorld() {
        return spawnBlackListWorld;
    }

    private static void initConfig() {
        config.ifPresent(conf -> {
            addDefault(conf.getConfig(),"debug", debug);

            addDefault(conf.getConfig(),"spawnCost", spawnCost);
            addDefault(conf.getConfig(),"spawnLocation", spawnLocation);
            addDefault(conf.getConfig(),"spawnTimeToExecute", spawnTimeToExecute);
            addDefault(conf.getConfig(),"spawnIsDenyWhenMoved", spawnIsDenyWhenMoved);
            addDefault(conf.getConfig(),"spawnBlackListedWorld", spawnBlackListWorld);

            addDefault(conf.getConfig(),"General.FirstJoin.Enabled", false);
            addDefault(conf.getConfig(),"General.FirstJoin.MultiServer", true);

            addDefault(conf.getConfig(),"Chat.isCustomChatEnable", Chat.isIsCustomChatEnable());
            addDefault(conf.getConfig(),"Chat.factionEnabled", Chat.isFactionEnabled());
            addDefault(conf.getConfig(),"Chat.isFactionDisplayEnabled", Chat.isIsFactionDisplayEnabled());
            addDefault(conf.getConfig(),"Chat.localChatEnable", Chat.isLocalChatEnable());
            addDefault(conf.getConfig(),"Chat.colorBendingEnabled", Chat.isColorBendingEnabled());
            addDefault(conf.getConfig(),"Chat.displayGroupEnabled", Chat.isDisplayGroupEnabled());
            addDefault(conf.getConfig(),"Chat.maxRange", Chat.getMaxRange());

            addDefault(conf.getConfig(),"Chat.MultiServer.enabled", Chat.isMultiServerEnabled());
            addDefault(conf.getConfig(),"Chat.MultiServer.Myslql.host", Chat.getHost());
            addDefault(conf.getConfig(),"Chat.MultiServer.Myslql.port", Chat.getPort());
            addDefault(conf.getConfig(),"Chat.MultiServer.Myslql.pass", Chat.getPass());
            addDefault(conf.getConfig(),"Chat.MultiServer.Myslql.db", Chat.getDb());
            addDefault(conf.getConfig(),"Chat.MultiServer.Myslql.user", Chat.getUser());
            addDefault(conf.getConfig(),"Chat.MultiServer.Save.server-uuid", Chat.getDbserverUuid());
        });
    }

    private static void addDefault(final YamlConfiguration config, final String path, final Object value) {
        if (config.get(path) == null)
            config.set(path, value);
    }

    public static Optional<IFileManager.IConfig> getConfig() {
        return config;
    }
}
