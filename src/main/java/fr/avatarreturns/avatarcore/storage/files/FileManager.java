package fr.avatarreturns.avatarcore.storage.files;

import fr.avatarreturns.api.storage.files.IFileManager;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class FileManager implements IFileManager {

    private static FileManager instance;

    public static FileManager getInstance() {
        if (instance == null)
            instance = new FileManager();
        return instance;
    }

    private final List<IConfig> files;

    private FileManager() {
        this.files = new ArrayList<>();
    }

    @Override
    public Optional<IConfig> createFile(final String path, final String name) {
        if (this.getLocalConfig(name).isPresent())
            return Optional.empty();
        final Config conf = new Config(path, name);
        final File file = new File(path, name);
        if (!file.exists()) {
            file.getParentFile().mkdirs();
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else
            conf.setFirstCreation(false);
        final YamlConfiguration yamlConfiguration = new YamlConfiguration();
        try {
            yamlConfiguration.load(file);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
        conf.setFile(file);
        conf.setConfig(yamlConfiguration);
        this.files.add(conf);
        return Optional.of(conf);
    }

    private Optional<IConfig> getLocalConfig(final String name) {
        return this.files.stream().filter(config -> ((Config) config).name.equals(name)).findFirst();
    }

    private static class Config implements IConfig {

        final String path;
        final String name;

        boolean firstCreation = true;
        YamlConfiguration config;
        File file;

        public Config(String path, String name) {
            this.path = path;
            this.name = name;
        }

        @Override
        public boolean firstCreation() {
            return this.firstCreation;
        }

        @Override
        public YamlConfiguration getConfig() {
            return this.config;
        }

        @Override
        public void save() {
            try {
                this.config.save(this.file);
                this.config.load(this.file);
            } catch (IOException | InvalidConfigurationException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void destroy() {
            this.file.delete();
        }

        public void setFirstCreation(boolean firstCreation) {
            this.firstCreation = firstCreation;
        }

        public void setConfig(YamlConfiguration config) {
            this.config = config;
        }

        public void setFile(File file) {
            this.file = file;
        }
    }
}
