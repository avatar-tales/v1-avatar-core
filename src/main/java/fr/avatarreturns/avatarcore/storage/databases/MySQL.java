package fr.avatarreturns.avatarcore.storage.databases;

import fr.avatarreturns.api.AvatarReturnsAPI;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;

public class MySQL extends Database {

    private String host = "localhost";
    private final String user;
    private final String password;
    private final String databaseName;
    private int port = 3306;

    public MySQL(final JavaPlugin plugin, final String host, final String user, final String password, final String databaseName, final int port) {
        super(plugin);
        this.host = host;
        this.user = user;
        this.password = password;
        this.databaseName = databaseName;
        this.port = port;
        this.open();
    }

    public MySQL(JavaPlugin plugin, String user, String password, String databaseName) {
        super(plugin);
        this.user = user;
        this.password = password;
        this.databaseName = databaseName;
        this.open();
    }

    @Override
    public Connection open() {
        try {
            Class.forName("com.mysql.jdbc.Driver");

            final String url = "jdbc:mysql://" + this.host + ":" + this.port + "/" + this.databaseName;

            this.connection = DriverManager.getConnection(url, this.user, this.password);
            AvatarReturnsAPI.get().log(Level.INFO, "Connection established!", this.plugin);

            return this.connection;
        } catch (final ClassNotFoundException e) {
            AvatarReturnsAPI.get().log(Level.SEVERE, "JDBC driver not found!", this.plugin);
            return null;
        } catch (final SQLException e) {
            e.printStackTrace();
            AvatarReturnsAPI.get().log(Level.SEVERE, "MYSQL exception during connection.", this.plugin);
            return null;
        }

    }
}
