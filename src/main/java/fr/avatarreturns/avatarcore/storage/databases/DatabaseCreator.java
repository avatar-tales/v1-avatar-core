package fr.avatarreturns.avatarcore.storage.databases;

import fr.avatarreturns.api.storage.databases.IDatabase;
import fr.avatarreturns.api.storage.databases.IDatabaseCreator;
import org.bukkit.plugin.java.JavaPlugin;

public class DatabaseCreator implements IDatabaseCreator {

    private static DatabaseCreator instance;

    public static DatabaseCreator getInstance() {
        if (instance == null)
            instance = new DatabaseCreator();
        return instance;
    }

    private DatabaseCreator() {
    }

    @Override
    public IDatabase createSQLiteDatabase(final JavaPlugin javaPlugin, final String path, final String name) {
        return new SQLite(javaPlugin, path, name);
    }

    @Override
    public IDatabase connectMySQLDatabase(final JavaPlugin javaPlugin, final String host, final String user, final String password, final String databaseName, final int port) {
        return new MySQL(javaPlugin, host, user, password, databaseName, port);
    }

}
