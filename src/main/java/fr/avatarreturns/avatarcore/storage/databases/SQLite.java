package fr.avatarreturns.avatarcore.storage.databases;

import fr.avatarreturns.api.AvatarReturnsAPI;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;

public class SQLite extends Database {

    private final String location;
    private final String database;
    private final File SQLfile;

    public SQLite(final JavaPlugin javaPlugin, final String path, final String name) {
        super(javaPlugin);
        this.database = name;
        this.location = path;
        final File folder = new File(this.location);
        if (!folder.exists()) {
            folder.mkdirs();
        }
        this.SQLfile = new File(folder.getAbsolutePath() + File.separator + this.database);
        this.open();
    }

    @Override
    public Connection open() {
        try {
            Class.forName("org.sqlite.JDBC");

            this.connection = DriverManager.getConnection("jdbc:sqlite:" + this.SQLfile.getAbsolutePath());
            AvatarReturnsAPI.get().log(Level.INFO, "Connection established!", this.plugin);

            return this.connection;
        } catch (final ClassNotFoundException e) {
            AvatarReturnsAPI.get().log(Level.SEVERE, "JDBC driver not found!", this.plugin);
            return null;
        } catch (final SQLException e) {
            AvatarReturnsAPI.get().log(Level.SEVERE, "SQLite exception during connection.", this.plugin);
            return null;
        }

    }

}
