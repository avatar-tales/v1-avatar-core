package fr.avatarreturns.avatarcore.storage.databases;

import fr.avatarreturns.api.storage.databases.IDatabase;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.*;
import java.util.Optional;

public abstract class Database implements IDatabase {

    protected JavaPlugin plugin;
    protected Connection connection = null;

    public Database(final JavaPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public void modifyQuery(final String query) {
        this.modifyQuery(query, true);
    }

    @Override
    public void modifyQuery(final String query, final boolean async) {
        if (async) {
            this.plugin.getServer().getScheduler().runTaskAsynchronously(
                    this.plugin,
                    () -> this.doQuery(query)
            );
        } else {
            this.doQuery(query);
        }
    }

    @Override
    public Optional<ResultSet> readQuery(final String query) {
        try {
            if (this.connection == null || this.connection.isClosed()) {
                this.open();
            }
            final PreparedStatement stmt = this.connection.prepareStatement(query);
            return Optional.ofNullable(stmt.executeQuery());
        } catch (final SQLException e) {
            e.printStackTrace();
            return Optional.empty();
        }

    }

    @Override
    public boolean tableExists(final String tableName) {
        try {
            if (this.connection == null || this.connection.isClosed()) {
                this.open();
            }
            final DatabaseMetaData dmd = this.connection.getMetaData();
            final ResultSet rs = dmd.getTables(null, null, tableName, null);

            return rs.next();
        } catch (final Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    @Override
    public boolean columnExists(final String tableName, final String columnName) {
        try {
            if (this.connection == null || this.connection.isClosed()) {
                this.open();
            }
            final DatabaseMetaData dmd = this.connection.getMetaData();
            final ResultSet rs = dmd.getColumns(null, null, tableName, columnName);
            return rs.next();
        } catch (final Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    @Override
    public boolean isOpen() {
        try {
            return (this.connection == null || this.connection.isClosed());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Connection getConnection() {
        return this.connection;
    }

    protected abstract Connection open();

    private synchronized void doQuery(final String query) {
        try {
            if (this.connection == null || this.connection.isClosed()) {
                this.open();
            }
            final PreparedStatement stmt = this.connection.prepareStatement(query);
            stmt.execute();
            stmt.close();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

}
